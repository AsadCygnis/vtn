import {actionGenerator} from '../../util/reduxHelpers';

export const REWARDS_GET = actionGenerator('REWARDS/REWARDS_GET');
