import * as constants from './constants';
import * as helpers from '../../util/reduxHelpers';

export const INITIAL_STATE = {
  byId: {
    1: {
      type: 'Money Transfer',
      amount: 10,
      createdAt: new Date(),
      user: {
        firstName: 'Arine',
        lastName: 'Ghea',
      },
    },
    2: {
      type: 'Money Transfer',
      amount: 5,
      createdAt: new Date(),
      user: {
        firstName: 'Bella',
        lastName: 'Silvana',
      },
    },
    3: {
      type: 'Money Transfer',
      amount: 12,
      createdAt: new Date(),
      user: {
        firstName: 'Kim',
        lastName: 'Stanley',
      },
    },
    4: {
      type: 'Money Transfer',
      amount: 4,
      createdAt: new Date(),
      user: {
        firstName: 'Robert',
        lastName: 'Sineo',
      },
    },
    5: {
      type: 'Money Transfer',
      amount: 35,
      createdAt: new Date(),
      user: {
        firstName: 'Geraldine',
        lastName: 'Anne',
      },
    },
    6: {
      type: 'Money Transfer',
      amount: 2,
      createdAt: new Date(),
      user: {
        firstName: 'Nelda',
        lastName: 'Kapoor',
      },
    },
  },
  allIds: [1, 2, 3, 4, 5, 6],
  error: null,
  loading: false,
};

export default function reducer(state = INITIAL_STATE, action) {
  const {type, payload, error} = action;

  switch (type) {
    // REWARDS_GET
    case constants.REWARDS_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.REWARDS_GET.FAIL:
      return {
        ...state,
        error,
      };
    case constants.REWARDS_GET.SUCCESS:
      return helpers.merge(state, payload);
    case constants.REWARDS_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
