import firebaseAuth from '@react-native-firebase/auth';

import * as constants from './constants';
import {RewardsCollection} from '../../config/firebase';

/**
 * REWARDS_GET
 */
export const getRewards = () => async dispatch => {
  try {
    dispatch({type: constants.REWARDS_GET.REQUEST});

    const uid = firebaseAuth().currentUser.uid;
    const rewardsSnapshot = await RewardsCollection.where(
      'user',
      '==',
      uid,
    ).get();

    const rewards = [];
    rewardsSnapshot.forEach(snapshot => {
      if (snapshot.exists) {
        rewards.push({
          id: snapshot.id,
          ...snapshot.data(),
        });
      }
    });

    dispatch({
      type: constants.REWARDS_GET.SUCCESS,
      payload: rewards,
    });
  } catch (error) {
    dispatch({type: constants.REWARDS_GET.FAIL, error});
  } finally {
    dispatch({type: constants.REWARDS_GET.COMPLETE});
  }
};
