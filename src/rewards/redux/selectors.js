import {createSelector} from 'reselect';
// import {firestoreTimestampToDate} from '../../util/functions';

/**
 * Get error
 */
export const getError = state => state.Rewards.error;

/**
 * Get loading
 */
export const getLoading = state => state.Rewards.loading;

/**
 * Get all ids
 */
export const getAllIds = state => state.Rewards.allIds;

/**
 * Get byId
 */
export const getById = state => state.Rewards.byId;

/**
 * Get sortOrder
 */
const getSortOrder = (state, {sortOrder}) => sortOrder;

/**
 * Get reward ids
 */
export const getRewardIdsByMonth = createSelector(
  [getAllIds, getById, getSortOrder],
  (allIds, byId, sortOrder) => {
    const rewardsByMonth = [
      {
        title: new Date(),
        data: allIds,
      },
    ];
    return rewardsByMonth;
  },
);

/**
 * Get id
 */
const getId = (state, {id}) => id;

/**
 * Get reward by id
 */
export const makeGetRewardById = () =>
  createSelector([getById, getId], (byId, id) => ({
    ...byId[id],
    // createdAt: byId[id].createdAt,
  }));
