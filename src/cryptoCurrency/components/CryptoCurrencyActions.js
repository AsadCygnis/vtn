import React from 'react';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';

import {Text, Touchable, View} from '../../common';
import ArrowUpIcon from '../../assets/icons/nav-arrow-up.svg';
import ArrowDownIcon from '../../assets/icons/nav-arrow-down.svg';
import KrakenIcon from '../../assets/icons/brand-kraken.svg';
import * as Colors from '../../config/colors';

/* =============================================================================
<CryptoCurrencyActions />
============================================================================= */
const CryptoCurrencyActions = () => {
  const _handleSendPress = () => {};

  const _handleReceivePress = () => {};

  const _handleBuySellPress = () => {};

  return (
    <View style={styles.container}>
      <Touchable style={styles.link} onPress={_handleSendPress}>
        <View style={styles.iconContainer}>
          <ArrowUpIcon fill={Colors.text} />
        </View>
        <Text style={styles.label}>
          <FormattedMessage defaultMessage="Send" />
        </Text>
      </Touchable>
      <Touchable style={styles.link} onPress={_handleReceivePress}>
        <View style={styles.iconContainer}>
          <ArrowDownIcon fill={Colors.text} />
        </View>
        <Text style={styles.label}>
          <FormattedMessage defaultMessage="Receive" />
        </Text>
      </Touchable>
      <Touchable style={styles.link} onPress={_handleBuySellPress}>
        <View style={styles.iconContainer}>
          <KrakenIcon />
        </View>
        <Text style={styles.label}>
          <FormattedMessage defaultMessage="Buy / Sell" />
        </Text>
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 16,
    paddingHorizontal: 15,
  },
  link: {
    alignItems: 'center',
  },
  iconContainer: {
    width: 55,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 2,
    backgroundColor: Colors.card,
  },
  label: {
    marginTop: 6,
    textAlign: 'center',
  },
});

/* Export
============================================================================= */
export default CryptoCurrencyActions;
