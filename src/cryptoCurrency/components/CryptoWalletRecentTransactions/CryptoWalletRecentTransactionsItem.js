import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {FormattedNumber, FormattedDate} from 'react-intl';

import {View, Text, Touchable} from '../../../common';
import * as Colors from '../../../config/colors';

/* =============================================================================
<CryptoWalletRecentTransactionsItem />
============================================================================= */
const CryptoWalletRecentTransactionsItem = ({id, transaction, altName}) => {
  const navigation = useNavigation();
  const avatar = transaction ? transaction.user.avatar : null;
  const name = transaction
    ? `${transaction.user.firstName} ${transaction.user.lastName}`
    : '';
  const date = transaction ? transaction.createdAt : '';
  const balance = transaction ? transaction.balance : '';
  const balanceUsd = transaction ? transaction.balanceUsd : '';

  const _handlePress = () => {
    navigation.navigate('CryptoWallet', {
      id,
    });
  };

  return (
    <Touchable style={styles.container} onPress={_handlePress}>
      <Image source={avatar} style={styles.avatar} />
      <View flex={1}>
        <View horizontal alignItems="center" justifyContent="space-between">
          <Text style={styles.date}>
            <FormattedDate value={date} />
          </Text>
          <Text style={styles.balanceUsd}>
            <FormattedNumber
              value={balanceUsd}
              currency="usd"
              style="currency"
              maximumFractionDigits={2}
            />
          </Text>
        </View>
        <View horizontal alignItems="center" justifyContent="space-between">
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.balance}>
            <FormattedNumber value={balance} maximumFractionDigits={2} />{' '}
            {altName}
          </Text>
        </View>
      </View>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6,
  },
  avatar: {
    width: 45,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 12,
    borderRadius: 10,
    backgroundColor: Colors.card,
  },
  date: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'left',
  },
  balanceUsd: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'right',
  },
  name: {
    color: Colors.text,
    textAlign: 'left',
  },
  balance: {
    color: Colors.primary,
    textAlign: 'right',
  },
});

/* Export
============================================================================= */
export default CryptoWalletRecentTransactionsItem;
