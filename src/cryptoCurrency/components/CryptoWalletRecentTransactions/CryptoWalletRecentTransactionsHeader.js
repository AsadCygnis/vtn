import React from 'react';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';

import {View, LinkButton, Text} from '../../../common';
import ExchangeIcon from '../../../assets/icons/edit-exchange.svg';
import * as Colors from '../../../config/colors';

/* =============================================================================
<CryptoWalletRecentTransactionsHeader />
============================================================================= */
const CryptoWalletRecentTransactionsHeader = () => {
  return (
    <View style={styles.container}>
      <LinkButton
        icon={<ExchangeIcon fill={Colors.background} />}
        title={<FormattedMessage defaultMessage="Exchange" />}
        containerStyle={styles.linkBtn}
      />
      <Text color={Colors.label} fontSize={10}>
        <FormattedMessage defaultMessage="Recent Transaction" />
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    paddingBottom: 6,
  },
  linkBtn: {
    marginBottom: 24,
  },
});

/* Export
============================================================================= */
export default CryptoWalletRecentTransactionsHeader;
