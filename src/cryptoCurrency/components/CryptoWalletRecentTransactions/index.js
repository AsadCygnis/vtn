import React from 'react';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import {View} from '../../../common';
import CryptoWalletRecentTransactionsHeader from './CryptoWalletRecentTransactionsHeader';
import CryptoWalletRecentTransactionsItem from './CryptoWalletRecentTransactionsItem';
import AvatarSample from '../../../assets/images/app-avatar-sample.png';

/* =============================================================================
<CryptoWalletRecentTransactions />
============================================================================= */
const CryptoWalletRecentTransactions = ({transactions}) => {
  return (
    <View style={styles.container}>
      <CryptoWalletRecentTransactionsHeader />
      {transactions.map(transaction => (
        <CryptoWalletRecentTransactionsItem
          key={transaction.id}
          currency={transaction}
          transaction={transaction}
        />
      ))}
    </View>
  );
};

const TRANSACTIONS = [
  {
    id: '1',
    balance: '1400',
    balanceUsd: '1399',
    createdAt: new Date(),
    user: {
      firstName: 'Marek',
      lastName: 'Stevansky',
      avatar: AvatarSample,
    },
  },
  {
    id: '2',
    balance: '1400',
    balanceUsd: '1399',
    createdAt: new Date(),
    user: {
      firstName: 'Marek',
      lastName: 'Stevansky',
      avatar: AvatarSample,
    },
  },
  {
    id: '3',
    balance: '1400',
    balanceUsd: '1399',
    createdAt: new Date(),
    user: {
      firstName: 'Marek',
      lastName: 'Stevansky',
      avatar: AvatarSample,
    },
  },
];

const styles = StyleSheet.create({
  container: {},
});

const mapStateToProps = state => ({
  transactions: TRANSACTIONS,
});

/* Export
============================================================================= */
export default connect(mapStateToProps)(CryptoWalletRecentTransactions);
