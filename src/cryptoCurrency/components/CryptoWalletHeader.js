import {useNavigation} from '@react-navigation/core';
import React from 'react';
import {StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {FormattedMessage, FormattedNumber} from 'react-intl';

import {View, Text, Touchable} from '../../common';
import ArrowLeftIcon from '../../assets/icons/nav-arrow-left.svg';
import EthereumIcon from '../../assets/icons/crypto-ETH.svg';
import * as Colors from '../../config/colors';

/* =============================================================================
<CryptoWalletHeader />
============================================================================= */
const CryptoWalletHeader = ({currency, wallet}) => {
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);
  const navigation = useNavigation();
  const name = currency ? currency.name : 'Ethereum';
  const icon = currency ? (
    currency.icon({style: styles.icon})
  ) : (
    <EthereumIcon style={styles.icon} />
  );
  const altName = currency ? currency.altName : 'ETH';
  const displayDecimals = currency ? +currency.displayDecimals : 2;
  const balance = wallet ? wallet.balance : 1.021;
  const address = wallet ? wallet.address : '0X8tr57s8...9843JFHDf';

  const _handleBackPress = () => {
    navigation.goBack();
  };

  return (
    <>
      <View style={styles.container}>
        <Touchable style={styles.backBtn} onPress={_handleBackPress}>
          <ArrowLeftIcon />
        </Touchable>
        <View style={styles.content}>
          <View style={styles.iconContainer}>{icon}</View>
          <Text style={styles.name}>
            <FormattedMessage
              defaultMessage="{currency} Wallet"
              values={{currency: name}}
            />
          </Text>
          <Text style={styles.balance}>
            <FormattedNumber
              value={balance}
              minimumFractionDigits={2}
              maximumFractionDigits={displayDecimals}
            />{' '}
            <Text>{altName}</Text>
          </Text>
        </View>
      </View>
      <View style={styles.addressContainer}>
        <View>
          <Text color={Colors.label} fontSize={10}>
            <FormattedMessage defaultMessage="Wallet Address" />
          </Text>
          <Text fontFamily="Poppins-SemiBold">{address}</Text>
        </View>
        <Touchable style={styles.copyBtn}>
          <Text
            color={Colors.secondary}
            fontFamily="Poppins-SemiBold"
            textTransform="uppercase">
            <FormattedMessage defaultMessage="Copy" />
          </Text>
        </Touchable>
      </View>
    </>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flexDirection: 'row',
      paddingTop: insets.top,
      paddingRight: 20,
    },
    backBtn: {
      paddingVertical: 20,
      paddingHorizontal: 20,
      zIndex: 2,
    },
    content: {
      flex: 1,
      paddingTop: 10,
      marginLeft: -35,
      alignItems: 'center',
      zIndex: 1,
    },
    iconContainer: {
      width: 78,
      height: 71,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 2,
      backgroundColor: Colors.card,
    },
    icon: {
      width: 25,
    },
    name: {
      marginTop: 6,
      color: Colors.label,
      textAlign: 'center',
      fontFamily: 'Poppins-SemiBold',
    },
    balance: {
      fontSize: 28,
      marginTop: 12,
      fontFamily: 'Poppins-Bold',
    },
    addressContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: 16,
      paddingHorizontal: 20,
      borderBottomWidth: 1,
      borderBottomColor: Colors.placeholder,
    },
  });

/* Export
============================================================================= */
export default CryptoWalletHeader;
