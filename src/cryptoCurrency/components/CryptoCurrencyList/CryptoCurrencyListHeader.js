import React from 'react';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';

import {View, Button, LinkButton} from '../../../common';
import PlusIcon from '../../../assets/icons/edit-plus.svg';
import * as Colors from '../../../config/colors';

/* =============================================================================
<CryptoCurrencyListHeader />
============================================================================= */
const CryptoCurrencyListHeader = ({cryptoType, onCryptoTypeChange}) => {
  return (
    <View style={styles.container}>
      <View style={styles.selectContainer}>
        <Button
          type={cryptoType === 'stableCoins' ? 'primary' : 'light'}
          title={<FormattedMessage defaultMessage="Stable Coins" />}
          style={styles.selectBtn}
          textStyle={styles.selectBtnTxt}
          onPress={() => onCryptoTypeChange('stableCoins')}
        />
        <Button
          type={cryptoType === 'crypto' ? 'primary' : 'light'}
          title={<FormattedMessage defaultMessage="Crypto" />}
          style={styles.selectBtn}
          textStyle={styles.selectBtnTxt}
          onPress={() => onCryptoTypeChange('crypto')}
        />
      </View>
      <LinkButton
        icon={<PlusIcon fill={Colors.background} />}
        title={
          cryptoType === 'stableCoins' ? (
            <FormattedMessage defaultMessage="Add Stable Coin" />
          ) : (
            <FormattedMessage defaultMessage="Add Crypto" />
          )
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    paddingBottom: 20,
  },
  selectContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 24,
    marginHorizontal: -6,
  },
  selectBtn: {
    width: null,
    height: 30,
    paddingHorizontal: 14,
    marginHorizontal: 6,
  },
  selectBtnTxt: {
    fontSize: 12,
  },
});

/* Export
============================================================================= */
export default CryptoCurrencyListHeader;
