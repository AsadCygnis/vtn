import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {connect} from 'react-redux';

import {Text, View} from '../../../common';
import CryptoCurrencyListHeader from './CryptoCurrencyListHeader';
import CryptoCurrencyListItem from './CryptoCurrencyListItem';
import TetherIcon from '../../../assets/icons/crypto-USDT.svg';
import USDCoinIcon from '../../../assets/icons/crypto-USDC.svg';
import PaxIcon from '../../../assets/icons/crypto-PAX.svg';
import EthereumIcon from '../../../assets/icons/crypto-ETH.svg';
import BitcoinIcon from '../../../assets/icons/crypto-BCH.svg';
import LitecoinIcon from '../../../assets/icons/crypto-LTC.svg';
import {FormattedMessage} from 'react-intl';

/* =============================================================================
<CryptoCurrencyList />
============================================================================= */
const CryptoCurrencyList = ({currencies, stableCoins}) => {
  const [cryptoType, setCryptoType] = useState('stableCoins');
  return (
    <View style={styles.container}>
      <CryptoCurrencyListHeader
        cryptoType={cryptoType}
        onCryptoTypeChange={setCryptoType}
      />
      {cryptoType === 'stableCoins' &&
        stableCoins.map(currency => (
          <CryptoCurrencyListItem key={currency.id} currency={currency} />
        ))}
      {cryptoType === 'crypto' &&
        currencies.map(currency => (
          <CryptoCurrencyListItem key={currency.id} currency={currency} />
        ))}
      <Text style={styles.description}>
        <FormattedMessage defaultMessage="consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation. quis nostrud exercitation. consectetur adipiscing elit." />
      </Text>
    </View>
  );
};

const STABLE_COINS = [
  {
    id: '1',
    name: 'Tether',
    altName: 'USDT',
    balance: '1400',
    balanceUsd: '1399',
    icon: <TetherIcon />,
  },
  {
    id: '2',
    name: 'USD Coin',
    altName: 'USDC',
    balance: '1400',
    balanceUsd: '1399',
    icon: <USDCoinIcon />,
  },
  {
    id: '3',
    name: 'Paxos Standard',
    altName: 'PAX',
    balance: '1400',
    balanceUsd: '1399',
    icon: <PaxIcon />,
  },
];

const CURRENCIES = [
  {
    id: '1',
    name: 'Ethereum',
    altName: 'ETH',
    balance: '1400',
    balanceUsd: '1399',
    icon: <EthereumIcon />,
  },
  {
    id: '2',
    name: 'Bitcoin Cash',
    altName: 'BCH',
    balance: '1400',
    balanceUsd: '1399',
    icon: <BitcoinIcon />,
  },
  {
    id: '3',
    name: 'Litecoin',
    altName: 'LTC',
    balance: '1400',
    balanceUsd: '1399',
    icon: <LitecoinIcon />,
  },
];

const styles = StyleSheet.create({
  container: {},
  description: {
    marginVertical: 60,
    fontSize: 10,
    lineHeight: 20,
    textAlign: 'center',
  },
});

const mapStateToProps = state => ({
  currencies: CURRENCIES,
  stableCoins: STABLE_COINS,
});

/* Export
============================================================================= */
export default connect(mapStateToProps)(CryptoCurrencyList);
