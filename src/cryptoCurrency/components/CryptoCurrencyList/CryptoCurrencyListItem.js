import React from 'react';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/core';
import {FormattedNumber} from 'react-intl';

import {View, Text, Touchable} from '../../../common';
import * as Colors from '../../../config/colors';

/* =============================================================================
<CryptoCurrencyListItem />
============================================================================= */
const CryptoCurrencyListItem = ({id, currency}) => {
  const navigation = useNavigation();
  const icon = currency ? currency.icon : null;
  const name = currency ? currency.name : '';
  const altName = currency ? currency.altName : '';
  const balance = currency ? currency.balance : '';
  const balanceUsd = currency ? currency.balanceUsd : '';

  const _handlePress = () => {
    navigation.navigate('CryptoWallet', {
      id,
    });
  };

  return (
    <Touchable style={styles.container} onPress={_handlePress}>
      <View style={styles.iconContainer}>{icon}</View>
      <View flex={1}>
        <View horizontal alignItems="center" justifyContent="space-between">
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.balanceUsd}>
            <FormattedNumber
              value={balanceUsd}
              currency="usd"
              style="currency"
              maximumFractionDigits={2}
            />
          </Text>
        </View>
        <View horizontal alignItems="center" justifyContent="space-between">
          <Text style={styles.altName}>{altName}</Text>
          <Text style={styles.balance}>
            <FormattedNumber value={balance} maximumFractionDigits={2} />{' '}
            {altName}
          </Text>
        </View>
      </View>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6,
  },
  iconContainer: {
    width: 44,
    height: 41,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 12,
    backgroundColor: Colors.card,
  },
  name: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'left',
  },
  balanceUsd: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'right',
  },
  altName: {
    color: Colors.text,
    fontSize: 12,
    textAlign: 'left',
  },
  balance: {
    color: Colors.text,
    fontSize: 12,
    textAlign: 'right',
  },
});

/* Export
============================================================================= */
export default CryptoCurrencyListItem;
