import React from 'react';
import {StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Content, Container} from '../../common';
import CryptoWalletHeader from '../components/CryptoWalletHeader';
import CryptoWalletRecentTransactions from '../components/CryptoWalletRecentTransactions';

/* =============================================================================
<CryptoWalletScreen />
============================================================================= */
const CryptoWalletScreen = () => {
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);
  return (
    <Container>
      <CryptoWalletHeader />
      <Content style={styles.content}>
        <CryptoWalletRecentTransactions />
      </Content>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    content: {
      paddingBottom: insets.bottom || 20,
      paddingHorizontal: 20,
    },
  });

/* Export
============================================================================= */
export default CryptoWalletScreen;
