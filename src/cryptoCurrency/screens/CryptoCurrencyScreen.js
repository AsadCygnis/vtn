import React from 'react';
import {FormattedMessage} from 'react-intl';
import {StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Content, Container, ModalHeader} from '../../common';
import CryptoCurrencyActions from '../components/CryptoCurrencyActions';
import CryptoCurrencyList from '../components/CryptoCurrencyList';

/* =============================================================================
<CryptoCurrencyScreen />
============================================================================= */
const CryptoCurrencyScreen = () => {
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);
  return (
    <Container>
      <ModalHeader
        title={<FormattedMessage defaultMessage="Cryptocurrency" />}
      />
      <Content style={styles.content}>
        <CryptoCurrencyActions />
        <CryptoCurrencyList />
      </Content>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    content: {
      paddingBottom: insets.bottom || 20,
      paddingHorizontal: 20,
    },
  });

/* Export
============================================================================= */
export default CryptoCurrencyScreen;
