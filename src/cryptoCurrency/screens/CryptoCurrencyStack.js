import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import CryptoCurrencyScreen from './CryptoCurrencyScreen';
import CryptoWalletScreen from './CryptoWalletScreen';

const Stack = createStackNavigator();

/* =============================================================================
<CryptoCurrencyStack />
============================================================================= */
const CryptoCurrencyStack = () => {
  return (
    <Stack.Navigator
      headerMode="none"
      screenOptions={TransitionPresets.SlideFromRightIOS}>
      <Stack.Screen
        name="CryptoCurrencyHome"
        component={CryptoCurrencyScreen}
      />
      <Stack.Screen name="CryptoWallet" component={CryptoWalletScreen} />
    </Stack.Navigator>
  );
};

/* Export
============================================================================= */
export default CryptoCurrencyStack;
