import React from 'react';
import {Provider} from 'react-redux';
import {StripeProvider} from '@stripe/stripe-react-native';

import AppNavigation from './navigation';
import configureStore from './redux/configureStore';

const store = configureStore();

/* =============================================================================
<App />
============================================================================= */
const App = () => {
  return (
    <StripeProvider publishableKey="pk_test_TYooMQauvdEDq54NiTphI7jx">
      <Provider store={store}>
        <AppNavigation />
      </Provider>
    </StripeProvider>
  );
};

/* Export
============================================================================= */
export default App;
