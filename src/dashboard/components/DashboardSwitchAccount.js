import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {FormattedMessage} from 'react-intl';

import {Text, View, Touchable} from '../../common';
import BriefcaseIcon from '../../assets/icons/edit-briefcase.svg';
import * as Colors from '../../config/colors';

/* =============================================================================
<DashboardSwitchAccount />
============================================================================= */
const DashboardSwitchAccount = ({onPress}) => {
  const _handleSwitchPress = () => {
    if (onPress) {
      onPress();
    }
  };

  return (
    <View style={styles.container}>
      <Text fontSize={10}>
        <FormattedMessage defaultMessage="You are currently using personal account, to create a business account, press the button below." />
        <Text style={styles.link}>
          <FormattedMessage defaultMessage="(Learn More)" />
        </Text>
      </Text>
      <Touchable style={styles.switchBtn} onPress={_handleSwitchPress}>
        <BriefcaseIcon />
        <Text style={styles.switchText}>
          <FormattedMessage defaultMessage="Switch to Business Account" />
        </Text>
        <FontAwesome5Icon name="times" size={12} color={Colors.label} />
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    marginHorizontal: -20,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: Colors.placeholder,
  },
  link: {
    color: Colors.primary,
    fontSize: 10,
    fontFamily: 'Poppins-Medium',
  },
  switchBtn: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 16,
    paddingVertical: 12,
    paddingHorizontal: 14,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderRadius: 4,
    borderColor: Colors.border,
    backgroundColor: Colors.card,
  },
  switchText: {
    flex: 1,
    marginHorizontal: 16,
    color: Colors.primary,
    fontSize: 14,
    fontFamily: 'Poppins-SemiBoldItalic',
  },
});

const mapStateToProps = state => ({});

/* Export
============================================================================= */
export default connect(mapStateToProps)(DashboardSwitchAccount);
