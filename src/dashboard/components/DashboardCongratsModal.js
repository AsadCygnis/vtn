import React from 'react';
import RNModal from 'react-native-modal';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';

import {View, Text, Button} from '../../common';
// import BackgroundImage from '../../assets/images/background-congrats.png';
import * as Colors from '../../config/colors';

// import {getReceivedReferralBonusModalVisible} from '../../auth/redux/selectors';
// import {  hideReferralBonusModal as hideReferralBonusModalAction } from '../../auth/redux/actions';

/* =============================================================================
<DashboardCongratsModal />
============================================================================= */
const DashboardCongratsModal = ({
  receivedReferralBonusModalVisible,
  hideReferralBonusModal,
}) => {
  const _handleClose = () => {
    hideReferralBonusModal();
  };

  return (
    <RNModal
      isVisible={receivedReferralBonusModalVisible}
      backdropColor={Colors.text}
      useNativeDriver
      backdropOpacity={0.26}
      onBackdropPress={_handleClose}
      onBackButtonPress={_handleClose}>
      <View style={styles.content}>
        {/* <Image style={styles.background} source={BackgroundImage} /> */}
        <Text style={styles.title}>
          <FormattedMessage defaultMessage="Congratulations" />
        </Text>
        <Text style={styles.text}>
          <FormattedMessage
            values={{amount: 5}}
            defaultMessage="You just received {amount, number, ::currency currency/USD} by entering valid referral code upon registration."
          />
        </Text>
        <Button
          title={<FormattedMessage defaultMessage="Great" />}
          style={styles.submitBtn}
          onPress={_handleClose}
        />
      </View>
    </RNModal>
  );
};

const styles = StyleSheet.create({
  content: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 20,
    marginHorizontal: 20,
    borderRadius: 14,
    backgroundColor: Colors.background,
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
  },
  title: {
    marginTop: 12,
    color: Colors.border,
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Poppins-SemiBold',
    zIndex: 2,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 34,
    color: Colors.border,
    textAlign: 'center',
    zIndex: 2,
  },
  submitBtn: {
    width: '100%',
    marginTop: 40,
    zIndex: 2,
  },
});

const mapStateToProps = state => ({
  // receivedReferralBonusModalVisible:
  //   getReceivedReferralBonusModalVisible(state),
});

const mapDispatchToProps = {
  // hideReferralBonusModal: hideReferralBonusModalAction,
};

/* Export
============================================================================= */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardCongratsModal);
