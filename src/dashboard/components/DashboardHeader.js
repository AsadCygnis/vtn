import React from 'react';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/core';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Image, StyleSheet} from 'react-native';

import {Avatar, Text, Touchable, View} from '../../common';
import AppLogo from '../../assets/images/app-logo.png';
import * as Colors from '../../config/colors';

import {getUser} from '../../auth/redux/selectors';

/* =============================================================================
<DashboardHeader />
============================================================================= */
const DashboardHeader = ({user}) => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const layout = {
    paddingTop: insets.top + 20,
  };
  const avatar = user ? user.photoURL : '';
  const username = user ? user.displayName : '';

  const _handleProfilePress = () => {
    navigation.navigate('Profile');
  };

  return (
    <View style={[styles.container, layout]}>
      <Image source={AppLogo} style={styles.logo} />
      {/* <AppLogo /> */}
      <Touchable style={styles.content} onPress={_handleProfilePress}>
        <Avatar uri={avatar} size={42} radius={8} />
        <View horizontal center paddingHorizontal={12}>
          <Text style={styles.username}>{username} </Text>
          <FontAwesome5Icon
            name="circle"
            size={7}
            solid
            color={Colors.danger}
          />
        </View>
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingBottom: 12,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: Colors.placeholder,
  },
  logo: {
    width: 130,
    height: 35,
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'flex-start',
    paddingTop: 12,
  },
  username: {
    marginRight: 4,
    fontFamily: 'Poppins-SemiBold',
  },
});

const mapStateToProps = state => ({
  user: getUser(state),
});

/* Export
============================================================================= */
export default connect(mapStateToProps)(DashboardHeader);
