import React, {useState} from 'react';
import {FormattedMessage} from 'react-intl';

import {Content, Container, View, ComingSoonModal} from '../../common';
import DashboardLink from '../components/DashboardLink';
import DashboardHeader from '../components/DashboardHeader';
import DashboardSwitchAccount from '../components/DashboardSwitchAccount';
import DashboardCongratsModal from '../components/DashboardCongratsModal';
import * as Colors from '../../config/colors';

import BellIcon from '../../assets/icons/edit-bell.svg';
import StatisticsIcon from '../../assets/icons/edit-statistics.svg';
import RateArrowIcon from '../../assets/icons/edit-rate-arrow.svg';
import PageDoubleIcon from '../../assets/icons/edit-page-double.svg';
import CryptoIcon from '../../assets/icons/edit-crypto.svg';
import PageIcon from '../../assets/icons/edit-page.svg';
import PieChartIcon from '../../assets/icons/edit-pie-chart.svg';
import TelevisionIcon from '../../assets/icons/edit-television.svg';
import CardIcon from '../../assets/icons/edit-card.svg';
import MailIcon from '../../assets/icons/edit-mail.svg';
import SettingsIcon from '../../assets/icons/edit-settings.svg';

/* =============================================================================
<DashboardScreen />
============================================================================= */
const DashboardScreen = () => {
  const [comingSoonModal, setComingSoonModal] = useState(false);

  const _toggleComingSoonModal = () => {
    setComingSoonModal(prevState => !prevState);
  };

  return (
    <Container>
      <DashboardHeader />
      <Content paddingHorizontal={20}>
        <DashboardSwitchAccount onPress={_toggleComingSoonModal} />
        <View paddingVertical={10}>
          <DashboardLink
            to="WhatsNew"
            icon={<BellIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="What's New?" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="Trading"
            icon={<StatisticsIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Trading" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="Rates"
            icon={<RateArrowIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Rates" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="AcquisitionContracts"
            icon={<PageDoubleIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Acquisition Contracts" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="CryptoCurrency"
            icon={<CryptoIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Crypto Currency" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="BillPayments"
            icon={<PageIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Bill Payments" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="BudgetingAndPlanning"
            icon={<PieChartIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Budgeting & Planning" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="Savings"
            icon={<TelevisionIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Savings" />}
            onPress={_toggleComingSoonModal}
          />
          <DashboardLink
            to="LinkedAccount"
            icon={<CardIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Linked Account" />}
          />
          <DashboardLink
            to="Referral"
            icon={<MailIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Referral" />}
          />
          <DashboardLink
            to="Settings"
            icon={<SettingsIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Advanced Settings" />}
          />
        </View>
        <DashboardCongratsModal />
        <ComingSoonModal
          visible={comingSoonModal}
          onCancel={_toggleComingSoonModal}
        />
      </Content>
    </Container>
  );
};

/* Export
============================================================================= */
export default DashboardScreen;
