import React from 'react';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {FormattedMessage} from 'react-intl';

import {View, LinkButton} from '../../common';
import PlusIcon from '../../assets/icons/edit-plus.svg';
// import PaypalIcon from '../../assets/icons/brand-paypal.svg';
import * as Colors from '../../config/colors';

/* =============================================================================
<LinedAccountHeader />
============================================================================= */
const LinedAccountHeader = () => {
  const navigation = useNavigation();

  const _handleLinkNewAccountPress = () => {
    navigation.navigate('LinkNewAccount');
  };

  // const _handleLinkPaypalAccountPress = () => {
  // };

  return (
    <View style={styles.container}>
      <LinkButton
        icon={<PlusIcon />}
        title={<FormattedMessage defaultMessage="Link New Account" />}
        containerStyle={styles.link}
        onPress={_handleLinkNewAccountPress}
      />
      {/* <LinkButton
        icon={<PaypalIcon />}
        title={<FormattedMessage defaultMessage="Link Paypal Account" />}
        containerStyle={styles.link}
        onPress={_handleLinkPaypalAccountPress}
      /> */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderBottomWidth: 6,
    borderColor: Colors.card,
  },
  link: {
    marginVertical: 8,
  },
});

/* Export
============================================================================= */
export default LinedAccountHeader;
