import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';

import {View, Text, ListEmpty} from '../../../common';
import LinkedAccountListItem from './LinkedAccountListItem';
import * as Colors from '../../../config/colors';

import {getAccountIds} from '../../redux/selectors';
import {getAccounts as getAccountsAction} from '../../redux/actions';

/* =============================================================================
<LinkedAccountList />
============================================================================= */
const LinkedAccountList = ({accounts, getAccounts}) => {
  useEffect(() => {
    getAccounts();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <View style={styles.container}>
      <Text color={Colors.label} fontSize={10} marginVertical={6}>
        <FormattedMessage defaultMessage="Linked Accounts" />
      </Text>
      {accounts.length ? (
        accounts.map(account => (
          <LinkedAccountListItem key={account} id={account} />
        ))
      ) : (
        <ListEmpty title={<FormattedMessage defaultMessage="No Data" />} />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 8,
    paddingHorizontal: 20,
  },
});

const mapStateToProps = state => ({
  accounts: getAccountIds(state),
});

const mapDispatchToProps = {
  getAccounts: getAccountsAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(LinkedAccountList);
