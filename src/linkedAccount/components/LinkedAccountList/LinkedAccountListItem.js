import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';

import {View, Text, Touchable} from '../../../common';
import CardIcon from '../../../assets/icons/edit-card.svg';
import * as Colors from '../../../config/colors';

import {makeGetAccountById} from '../../redux/selectors';

/* =============================================================================
<LinkedAccountListItem />
============================================================================= */
const LinkedAccountListItem = ({account}) => {
  const title = account ? `${account.brand} *${account.last4}` : '';

  const _handlePress = () => {};

  return (
    <Touchable style={styles.container} onPress={_handlePress}>
      <View style={styles.iconContainer}>
        <CardIcon fill={Colors.primary} style={{fill: Colors.primary}} />
      </View>
      <Text fontFamily="Poppins-SemiBold">{title}</Text>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 6,
  },
  iconContainer: {
    width: 44,
    height: 41,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 12,
    paddingVertical: 0,
    paddingHorizontal: 0,
    borderRadius: 4,
    backgroundColor: Colors.card,
  },
});

const mapStateToProps = () => {
  const getAccountById = makeGetAccountById();
  return (state, {id}) => ({
    account: getAccountById(state, {id}),
  });
};

export default connect(mapStateToProps)(LinkedAccountListItem);
