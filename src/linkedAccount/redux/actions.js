import * as constants from './constants';
import request from '../../util/request';

/**
 * ACCOUNTS_GET
 */
export const getAccounts = () => async dispatch => {
  try {
    dispatch({type: constants.ACCOUNTS_GET.REQUEST});

    const payload = await request({
      url: '/linked_account',
      method: 'GET',
    });

    dispatch({
      type: constants.ACCOUNTS_GET.SUCCESS,
      payload,
    });
  } catch (error) {
    dispatch({type: constants.ACCOUNTS_GET.FAIL, error});
  } finally {
    dispatch({type: constants.ACCOUNTS_GET.COMPLETE});
  }
};
