import {actionGenerator} from '../../util/reduxHelpers';

export const ACCOUNTS_GET = actionGenerator('LINKED_ACCOUNT/ACCOUNTS_GET');
