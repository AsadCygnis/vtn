import {createSelector} from 'reselect';
// import {firestoreTimestampToDate} from '../../util/functions';

/**
 * Get error
 */
export const getError = state => state.LinkedAccount.error;

/**
 * Get loading
 */
export const getLoading = state => state.LinkedAccount.loading;

/**
 * Get all ids
 */
export const getAllIds = state => state.LinkedAccount.allIds;

/**
 * Get byId
 */
export const getById = state => state.LinkedAccount.byId;

/**
 * Get account ids
 */
export const getAccountIds = createSelector(
  [getAllIds, getById],
  (allIds, byId) =>
    allIds.slice().sort((a, b) => byId[a].createdAt - byId[b].createdAt),
);

/**
 * Get id
 */
const getId = (state, {id}) => id;

/**
 * Get account by id
 */
export const makeGetAccountById = () =>
  createSelector([getById, getId], (byId, id) => ({
    ...byId[id],
    // createdAt: byId[id].createdAt,
  }));
