import * as constants from './constants';
import * as helpers from '../../util/reduxHelpers';

export const INITIAL_STATE = {
  byId: {},
  allIds: [],
  error: null,
  loading: false,
};

export default function reducer(state = INITIAL_STATE, action) {
  const {type, payload, error} = action;

  switch (type) {
    // ACCOUNTS_GET
    case constants.ACCOUNTS_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.ACCOUNTS_GET.FAIL:
      return {
        ...state,
        error,
      };
    case constants.ACCOUNTS_GET.SUCCESS:
      return helpers.normalize(state, payload);
    case constants.ACCOUNTS_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
