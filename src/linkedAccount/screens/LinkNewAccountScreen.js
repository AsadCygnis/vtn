import React, {useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {CardField, useConfirmSetupIntent} from '@stripe/stripe-react-native';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {
  View,
  Text,
  Button,
  Content,
  Container,
  StackHeader,
} from '../../common';
import request from '../../util/request';
import * as Colors from '../../config/colors';

import {getAccounts as getAccountsAction} from '../redux/actions';

/* =============================================================================
<LinkNewAccountScreen />
============================================================================= */
const LinkNewAccountScreen = ({getAccounts}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);
  const navigation = useNavigation();
  const [fetching, setFetching] = useState(false);
  const [infoCollected, setInfoCollected] = useState(false);
  const {confirmSetupIntent, loading} = useConfirmSetupIntent();

  const _handleOnCardChange = ({complete}) => {
    setInfoCollected(complete);
  };

  const _handleContinuePress = async () => {
    if (!infoCollected) {
      return;
    }

    setFetching(true);
    try {
      const {clientSecret} = await request({
        url: '/linked_account/setup',
        method: 'post',
      });

      const {error} = await confirmSetupIntent(clientSecret, {
        type: 'Card',
      });

      if (error) {
        throw error;
      }

      getAccounts();

      Alert.alert(
        intl.formatMessage({defaultMessage: 'Success'}),
        intl.formatMessage({
          defaultMessage: 'Account linked successfully.',
        }),
        [
          {
            text: intl.formatMessage({defaultMessage: 'Ok'}),
            onPress: () => {
              navigation.goBack();
            },
          },
        ],
      );
    } catch (e) {
      Alert.alert('Error', e.message);
    }
    setFetching(false);
  };

  return (
    <Container>
      <StackHeader
        title={intl.formatMessage({defaultMessage: 'Link New Account'})}
      />
      <KeyboardAvoidingView
        style={styles.safeArea}
        behavior={Platform.select({ios: 'padding', android: undefined})}>
        <Content paddingVertical={8} paddingHorizontal={20}>
          <View flex={1}>
            <CardField
              style={styles.field}
              cardStyle={styles.card}
              postalCodeEnabled={false}
              onCardChange={_handleOnCardChange}
            />
            <Text color={Colors.label} fontSize={10} marginVertical={20}>
              {intl.formatMessage({
                defaultMessage:
                  'By clicking Continue, you agree and understand to our Privacy Policy and Term of Services.',
              })}
            </Text>
          </View>
          <Button
            title={intl.formatMessage({defaultMessage: 'Continue'})}
            style={styles.continueBtn}
            loading={fetching || loading}
            onPress={_handleContinuePress}
          />
        </Content>
      </KeyboardAvoidingView>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    safeArea: {
      flex: 1,
    },
    field: {
      width: '100%',
      height: 50,
      marginTop: 16,
      paddingHorizontal: 14,
      borderWidth: 0.5,
      borderRadius: 4,
      backgroundColor: Colors.card,
    },
    card: {
      marginTop: 12,
    },
    continueBtn: {
      marginBottom: insets.bottom || 20,
    },
  });

const mapDispatchToProps = {
  getAccounts: getAccountsAction,
};

/* Export
============================================================================= */
export default connect(null, mapDispatchToProps)(LinkNewAccountScreen);
