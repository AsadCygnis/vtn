import React from 'react';
import {FormattedMessage} from 'react-intl';

import {Content, Container, ModalHeader} from '../../common';
import LinkedAccountHeader from '../components/LinkedAccountHeader';
import LinkedAccountList from '../components/LinkedAccountList';

/* =============================================================================
<LinkedAccountScreen />
============================================================================= */
const LinkedAccountScreen = () => {
  return (
    <Container>
      <ModalHeader
        title={<FormattedMessage defaultMessage="Linked Account" />}
      />
      <Content>
        <LinkedAccountHeader />
        <LinkedAccountList />
      </Content>
    </Container>
  );
};

/* Export
============================================================================= */
export default LinkedAccountScreen;
