import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import LinkedAccountScreen from './LinkedAccountScreen';
import LinkNewAccountScreen from './LinkNewAccountScreen';

const Stack = createStackNavigator();

/* =============================================================================
<LinkedAccountStack />
============================================================================= */
const LinkedAccountStack = () => {
  return (
    <Stack.Navigator
      headerMode="none"
      screenOptions={TransitionPresets.SlideFromRightIOS}>
      <Stack.Screen name="LinkedAccountHome" component={LinkedAccountScreen} />
      <Stack.Screen name="LinkNewAccount" component={LinkNewAccountScreen} />
    </Stack.Navigator>
  );
};

/* Export
============================================================================= */
export default LinkedAccountStack;
