import React from 'react';
import {useIntl} from 'react-intl';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import TabBar from './TabBar';
import {View} from '../common';
import DashboardScreen from '../dashboard/screens/DashboardScreen';

const Tab = createBottomTabNavigator();

const EmptyScreen = () => <View flex={1} backgroundColor="#fff" />;

/* =============================================================================
<HomeTab />
============================================================================= */
const HomeTab = () => {
  const intl = useIntl();

  return (
    <Tab.Navigator tabBar={props => <TabBar {...props} />}>
      <Tab.Screen
        name={intl.formatMessage({defaultMessage: 'Dashboard'})}
        component={DashboardScreen}
      />
      <Tab.Screen
        name={intl.formatMessage({defaultMessage: 'Cards'})}
        component={EmptyScreen}
      />
      <Tab.Screen
        name={intl.formatMessage({defaultMessage: 'Transfers'})}
        component={EmptyScreen}
      />
      <Tab.Screen
        name={intl.formatMessage({defaultMessage: 'Accounts'})}
        component={EmptyScreen}
      />
      <Tab.Screen
        name={intl.formatMessage({defaultMessage: 'Analytics'})}
        component={EmptyScreen}
      />
    </Tab.Navigator>
  );
};

export default HomeTab;
