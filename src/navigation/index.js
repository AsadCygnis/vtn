import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import firebaseAuth from "@react-native-firebase/auth";
import { IntlProvider } from "react-intl";
import RNSplashScreen from "react-native-splash-screen";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { InteractionManager } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import { Statusbar } from "../common";
import HomeTab from "./HomeTab";
import en from "../lang/en.json";
import * as Colors from "../config/colors";

import AuthStack from "../auth/screens/AuthStack";
import ProfileScreen from "../contacts/screens/ProfileScreen";
import CryptoCurrencyStack from "../cryptoCurrency/screens/CryptoCurrencyStack";
import LinkedAccountStack from "../linkedAccount/screens/LinkedAccountStack";
import SettingsStack from "../settings/screens/SettingsStack";
import ReferralStack from "../referral/screens/ReferralStack";

import { getFullyAuthenticated } from "../auth/redux/selectors";
import {
  getUser as getUserAction,
  getAppSettings as getAppSettingsAction,
  getNotifications as getNotificationsAction,
} from "../settings/redux/actions";
import {
  getSecurity as getSecurityAction,
  changeAuthState as changeAuthStateAction,
} from "../auth/redux/actions";

const Stack = createStackNavigator();

/* =============================================================================
<AppNavigation />
============================================================================= */
const AppNavigation = ({
  locale,
  authenticated,
  changeAuthState,
  getSecurity,
  getUser,
  getAppSettings,
  getNotifications,
}) => {
  const [initializing, setInitializing] = useState(true);

  /**
   * Firebase authentication listener
   */
  useEffect(() => {
    return firebaseAuth().onAuthStateChanged(async (user) => {
      if (user) {
        await Promise.all([
          getSecurity(),
          getUser(),
          getAppSettings(),
          getNotifications(),
        ]);
      }
      changeAuthState(user && user.toJSON());
      setInitializing(false);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Hide splash screen
  useEffect(() => {
    if (!initializing) {
      InteractionManager.runAfterInteractions(() => {
        RNSplashScreen.hide();
      });
    }
  }, [initializing]);

  return (
    <IntlProvider
      locale={locale || "en"}
      messages={getTranslationsForLocale(locale)}
    >
      <SafeAreaProvider>
        <NavigationContainer theme={THEME}>
          <Statusbar />
          <Stack.Navigator
            headerMode="none"
            screenOptions={TransitionPresets.SlideFromRightIOS}
          >
            {true ? (
              <>
                <Stack.Screen name="Home" component={HomeTab} />
                <Stack.Screen name="Profile" component={ProfileScreen} />
                <Stack.Screen
                  name="CryptoCurrency"
                  component={CryptoCurrencyStack}
                />
                <Stack.Screen
                  name="LinkedAccount"
                  component={LinkedAccountStack}
                />
                <Stack.Screen name="Referral" component={ReferralStack} />
                <Stack.Screen name="Settings" component={SettingsStack} />
              </>
            ) : (
              <Stack.Screen name="Auth" component={AuthStack} />
            )}
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </IntlProvider>
  );
};

const THEME = {
  dark: false,
  colors: {
    primary: Colors.primary,
    background: Colors.background,
    card: Colors.card,
    text: Colors.text,
    border: Colors.border,
    notification: Colors.secondary,
  },
};

const getTranslationsForLocale = (locale) => {
  switch (locale) {
    default:
      return en;
  }
};

const mapStateToProps = (state) => ({
  // locale: selectAppSettings(state).language,
  authenticated: getFullyAuthenticated(state),
});

const mapDispatchToProps = {
  changeAuthState: changeAuthStateAction,
  getSecurity: getSecurityAction,
  getUser: getUserAction,
  getAppSettings: getAppSettingsAction,
  getNotifications: getNotificationsAction,
};

const propsAreEqual = (prevProps, nextProps) =>
  prevProps.locale === nextProps.locale &&
  prevProps.authenticated === nextProps.authenticated;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(AppNavigation, propsAreEqual));
