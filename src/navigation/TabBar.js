import React from 'react';
import {StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {View, Touchable, Text} from '../common';
import DashboardIcon from '../assets/icons/tab-dashboard.svg';
import CardsIcon from '../assets/icons/tab-cards.svg';
import TransfersIcon from '../assets/icons/tab-transfers.svg';
import AccountsIcon from '../assets/icons/tab-accounts.svg';
import AnalysisIcon from '../assets/icons/tab-analysis.svg';
import * as Colors from '../config/colors';

/* =============================================================================
<TabBar />
============================================================================= */
const TabBar = ({state, descriptors, navigation}) => {
  const insets = useSafeAreaInsets();
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  const containerStyle = {
    height: insets.bottom + 60,
    paddingBottom: insets.bottom,
  };

  return (
    <View style={[styles.container, containerStyle]}>
      {state.routes.map((route, index) => {
        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <Touchable
            key={route.name}
            style={[styles.item]}
            onPress={onPress}
            onLongPress={onLongPress}>
            {ICONS[index]({fill: isFocused ? Colors.primary : Colors.border})}
            <Text
              color={isFocused ? Colors.primary : Colors.border}
              style={styles.text}>
              {route.name}
            </Text>
          </Touchable>
        );
      })}
    </View>
  );
};

const ICONS = {
  0: DashboardIcon,
  1: CardsIcon,
  2: TransfersIcon,
  3: AccountsIcon,
  4: AnalysisIcon,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: Colors.background,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16,
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    marginTop: 4,
    fontSize: 8,
  },
});

export default TabBar;
