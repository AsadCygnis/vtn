import axios from 'axios';
import firebaseAuth from '@react-native-firebase/auth';

/**
 * Request Wrapper with default success/error actions
 */
const request = async options => {
  const token = await firebaseAuth().currentUser.getIdToken();

  const client = axios.create({
    baseURL: 'https://us-central1-thevtn-2474a.cloudfunctions.net/api',
    // baseURL: 'http://localhost:5001/thevtn-2474a/us-central1/api',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  const onSuccess = response => {
    return response.data;
  };

  const onError = error => {
    // console.debug('Request Failed:', error.config);

    if (error.response) {
      // Request was made but server responded with something
      // other than 2xx
      // console.debug('Status:', error.response.status);
      // console.debug('Data:', error.response.data);
      // console.debug('Headers:', error.response.headers);
    } else {
      // Something else happened while setting up the request
      // triggered the error
      // console.debug('Error Message:', error.message);
    }

    return Promise.reject(error.response ? error.response.data : error);
  };

  return client(options).then(onSuccess).catch(onError);
};

export default request;
