import firestore from '@react-native-firebase/firestore';

export const UsersCollection = firestore().collection('users');
