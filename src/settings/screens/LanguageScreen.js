import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import LanguageItem from '../components/LanguageItem';
import {Content, Container, StackHeader, View, Button} from '../../common';

import {
  getLoading,
  getAppSettings as selectAppSettings,
} from '../redux/selectors';
import {
  getAppSettings as getAppSettingsAction,
  updateAppSettings as updateAppSettingsAction,
} from '../redux/actions';

/* =============================================================================
<LanguageScreen />
============================================================================= */
const LanguageScreen = ({
  loading,
  language,
  getAppSettings,
  updateAppSettings,
}) => {
  const insets = useSafeAreaInsets();
  const [_language, setLanguage] = useState('en');
  const styles = getStyles(insets);
  const disabled = loading || !_language;

  // Get app language
  useEffect(() => {
    getAppSettings();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Sync language
  useEffect(() => {
    setLanguage(language);
  }, [language]);

  const _handleUpdateInformation = () => {
    if (!disabled) {
      updateAppSettings({
        language: _language,
      });
    }
  };

  return (
    <Container>
      <StackHeader title={<FormattedMessage defaultMessage="Language" />} />
      <Content>
        <View flex={1} paddingHorizontal={20}>
          <LanguageItem
            title={
              <FormattedMessage defaultMessage="English (United States)" />
            }
            name="en"
            value={_language}
            onPress={() => setLanguage('en')}
          />
        </View>
        <Button
          style={styles.updateBtn}
          title={<FormattedMessage defaultMessage="Update" />}
          loading={loading}
          disabled={disabled}
          onPress={_handleUpdateInformation}
        />
      </Content>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    updateBtn: {
      marginHorizontal: 20,
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
  language: selectAppSettings(state).language,
});

const mapDispatchToProps = {
  getAppSettings: getAppSettingsAction,
  updateAppSettings: updateAppSettingsAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(LanguageScreen);
