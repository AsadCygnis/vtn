import React, {useEffect, useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/core';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {
  View,
  Text,
  Touchable,
  Content,
  CodeInput,
  StackHeader,
} from '../../common';
import * as AppColors from '../../config/colors';

import {getLoading} from '../../auth/redux/selectors';
import {
  verifyPhoneNumber as verifyPhoneNumberAction,
  updatePhoneNumber as updatePhoneNumberAction,
} from '../../auth/redux/actions';

/* =============================================================================
<PhoneVerificationScreen />
============================================================================= */
const PhoneVerificationScreen = ({
  route,
  loading,
  verifyPhoneNumber,
  updatePhoneNumber,
}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [_code, setCode] = useState();
  const [resendTime, setResendTime] = useState(25);
  const [_verificationId, setVerificationId] = useState();
  const {code, phone, verificationId} = route.params;
  const phoneNumber = phone ? `${phone.code}${phone.number}` : '';
  const styles = getStyles(insets);

  // Sync state with props
  useEffect(() => {
    setCode(code);
    setVerificationId(verificationId);
  }, [verificationId, code]);

  // Register verified phone number
  useEffect(() => {
    if (_verificationId && _code) {
      updatePhoneNumber(_verificationId, _code, phone, _handleFinish);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_verificationId, _code]);

  // Start resend timer
  useEffect(() => {
    if (!verificationId && !code) {
      const interval = setInterval(() => {
        setResendTime(prevState => (prevState > 0 ? prevState - 1 : prevState));
      }, 1000);

      return () => {
        clearTimeout(interval);
      };
    }
  }, [verificationId, code]);

  const _handleFulfill = otp => {
    updatePhoneNumber(_verificationId, otp, phone, _handleFinish);
  };

  const _handleResend = async () => {
    if (phoneNumber) {
      verifyPhoneNumber(phoneNumber, true, confirmation => {
        setCode(confirmation.code);
        setVerificationId(confirmation.verificationId);
        setResendTime(25);
      });
    } else {
      Alert.alert('Error', 'Please enter a valid phone number');
    }
  };

  const _handleFinish = () => {
    Alert.alert(
      intl.formatMessage({defaultMessage: 'Success'}),
      intl.formatMessage({
        defaultMessage: 'Phone number updated successfully.',
      }),
      [
        {
          text: intl.formatMessage({defaultMessage: 'Ok'}),
          onPress: () => {
            navigation.navigation('MyAccount');
          },
        },
      ],
      {
        cancelable: false,
      },
    );
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.select({ios: 'padding', android: undefined})}>
      <StackHeader
        title={intl.formatMessage({defaultMessage: 'Verify Mobile Number'})}
      />
      <Content paddingHorizontal={20}>
        <View style={styles.form}>
          <CodeInput
            codeLength={6}
            style={styles.codeInput}
            onFulfill={_handleFulfill}
          />
        </View>
        <Touchable
          style={styles.resendTime}
          disabled={loading || resendTime > 0}
          onPress={_handleResend}>
          {resendTime ? (
            <>
              <Text>{intl.formatMessage({defaultMessage: 'Resend In: '})}</Text>
              <Text marginHorizontal={2} color={AppColors.danger}>
                00:{resendTime < 10 ? `0${resendTime}` : resendTime}
              </Text>
            </>
          ) : (
            <Text>{intl.formatMessage({defaultMessage: 'Resend'})}</Text>
          )}
        </Touchable>
      </Content>
    </KeyboardAvoidingView>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      marginTop: 12,
    },
    codeInput: {
      marginTop: 10,
    },
    resendTime: {
      flexDirection: 'row',
      alignSelf: 'center',
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
});

const mapDispatchToProps = {
  verifyPhoneNumber: verifyPhoneNumberAction,
  updatePhoneNumber: updatePhoneNumberAction,
};

/* Export
============================================================================= */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhoneVerificationScreen);
