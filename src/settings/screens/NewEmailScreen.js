import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useIntl} from 'react-intl';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {View, Button, TextInput, Content, ModalHeader} from '../../common';

import {getLoading} from '../../auth/redux/selectors';
import {updateEmail as updateEmailAction} from '../../auth/redux/actions';

/* =============================================================================
<NewEmailScreen />
============================================================================= */
const NewEmailScreen = ({loading, updateEmail}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const disabled = !email || loading;
  const styles = getStyles(insets);

  const _handleSubmit = () => {
    if (!disabled) {
      updateEmail(email, () => {
        Alert.alert(
          intl.formatMessage({defaultMessage: 'Success'}),
          intl.formatMessage({
            defaultMessage: 'Email updated successfully.',
          }),
          [
            {
              text: intl.formatMessage({defaultMessage: 'Ok'}),
              onPress: () => {
                navigation.navigation('MyAccount');
              },
            },
          ],
          {
            cancelable: false,
          },
        );
      });
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.keyboardView}
      behavior={Platform.select({ios: 'padding', android: undefined})}>
      <ModalHeader
        title={intl.formatMessage({defaultMessage: 'Setup Email'})}
      />
      <Content>
        <View style={styles.form}>
          <TextInput
            label={intl.formatMessage({
              defaultMessage: 'New Email',
            })}
            value={email}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter New Email',
            })}
            keyboardType="email-address"
            onChange={setEmail}
          />
        </View>
        <Button
          title={intl.formatMessage({defaultMessage: 'Save Email'})}
          style={styles.submitBtn}
          loading={loading}
          disabled={disabled}
          onPress={_handleSubmit}
        />
      </Content>
    </KeyboardAvoidingView>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    keyboardView: {
      flex: 1,
    },
    form: {
      flex: 1,
      paddingHorizontal: 20,
    },
    submitBtn: {
      marginTop: 20,
      marginHorizontal: 20,
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
});

const mapDispatchToProps = {
  updateEmail: updateEmailAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(NewEmailScreen);
