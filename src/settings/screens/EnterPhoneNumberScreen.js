import React, {useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {View, Button, Content, PhoneInput, ModalHeader} from '../../common';

import {getLoading} from '../../auth/redux/selectors';
import {verifyPhoneNumber as verifyPhoneNumberAction} from '../../auth/redux/actions';

/* =============================================================================
<EnterPhoneNumberScreen />
============================================================================= */
const EnterPhoneNumberScreen = ({loading, verifyPhoneNumber}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [number, setNumber] = useState('');
  const [dialCode, setDialCode] = useState('+1');
  const disabled = !dialCode || !number || loading;
  const styles = getStyles(insets);

  const _handleNext = () => {
    if (!disabled) {
      verifyPhoneNumber(`${dialCode}${number}`, false, confirmation => {
        navigation.navigate('PhoneVerification', {
          code: confirmation.code,
          phone: {
            code: dialCode,
            number,
          },
          verificationId: confirmation.verificationId,
        });
      });
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.select({ios: 'padding', android: undefined})}>
      <ModalHeader
        title={intl.formatMessage({defaultMessage: 'Enter Mobile Number'})}
      />
      <Content paddingHorizontal={20}>
        <View style={styles.form}>
          <PhoneInput
            value={number}
            dialCode={dialCode}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter Phone Number',
            })}
            keyboardType="number-pad"
            onChange={setNumber}
            onDialCodeChange={setDialCode}
          />
        </View>
        <Button
          title={intl.formatMessage({defaultMessage: 'Next'})}
          style={styles.submitBtn}
          loading={loading}
          disabled={disabled}
          onPress={_handleNext}
        />
      </Content>
    </KeyboardAvoidingView>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      marginTop: 12,
    },
    submitBtn: {
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
});

const mapDispatchToProps = {
  verifyPhoneNumber: verifyPhoneNumberAction,
};

/* Export
============================================================================= */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EnterPhoneNumberScreen);
