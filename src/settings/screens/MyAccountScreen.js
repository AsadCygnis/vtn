import React, {useEffect, useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {
  Content,
  Container,
  StackHeader,
  View,
  Button,
  TextInput,
  DateTimePicker,
  Select,
  PhoneInput,
  Text,
  Touchable,
  ReAuthModal,
} from '../../common';
import * as Colors from '../../config/colors';
import {firestoreTimestampToDate} from '../../util/functions';

import {getLoading, getUser as selectUser} from '../redux/selectors';
import {
  getUser as getUserAction,
  updateUser as updateUserAction,
} from '../redux/actions';

/* =============================================================================
<MyAccountScreen />
============================================================================= */
const MyAccountScreen = ({user, loading, getUser, updateUser}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const styles = getStyles(insets);
  const [gender, setGender] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [dob, setDob] = useState('');
  const [email, setEmail] = useState('');
  const [socialIssuanceNumber, setSocialIssuanceNumber] = useState('');
  const [dialCode, setDialCode] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [address, setAddress] = useState('');
  const [linkToOpen, setLinkToOpen] = useState('');
  const [reAuthVisible, setReAuthVisible] = useState(false);
  const [reAuthenticatedAt, setReAuthenticatedAt] = useState(null);
  const disabled =
    !firstName || !lastName || !dob || !gender || !email || loading;
  const genderData = [
    {
      label: intl.formatMessage({defaultMessage: 'Mr'}),
      value: 'male',
    },
    {
      label: intl.formatMessage({defaultMessage: 'Mrs'}),
      value: 'female',
    },
  ];

  // Get user
  useEffect(() => {
    getUser();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Sync user values
  useEffect(() => {
    if (user) {
      setGender(user.gender);
      setFirstName(user.firstName);
      setLastName(user.lastName);
      setDob(firestoreTimestampToDate(user.dob));
      setEmail(user.email);
      setSocialIssuanceNumber(user.socialIssuanceNumber || '');
      setDialCode(user.phone.code);
      setPhoneNumber(user.phone.number);
      setAddress(user.address || '');
    }
  }, [user]);

  const _toggleReAuthVisible = () => {
    setReAuthVisible(prevState => !prevState);
  };

  const _handleReAuthFinish = () => {
    _toggleReAuthVisible();
    setReAuthenticatedAt(Date.now());
    if (linkToOpen) {
      navigation.navigate(linkToOpen);
      setLinkToOpen('');
    }
  };

  const _handleChangePress = link => event => {
    event.stopPropagation();
    if (reAuthenticatedAt && (Date.now() - reAuthenticatedAt) / 1000 < 60) {
      navigation.navigate(link);
    } else {
      _toggleReAuthVisible();
      setLinkToOpen(link);
    }
  };

  const _handleUpdatePress = () => {
    if (!disabled) {
      updateUser(
        {
          gender,
          firstName,
          lastName,
          dob,
          socialIssuanceNumber,
          address,
        },
        () => {
          Alert.alert(
            intl.formatMessage({defaultMessage: 'Success'}),
            intl.formatMessage({
              defaultMessage: 'Information updated successfully.',
            }),
          );
        },
      );
    }
  };

  return (
    <Container>
      <StackHeader title={intl.formatMessage({defaultMessage: 'My Account'})} />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.select({ios: 'padding', android: undefined})}>
        <Content>
          <View style={styles.form}>
            <View style={styles.row}>
              <Select
                data={genderData}
                label={intl.formatMessage({defaultMessage: 'Title'})}
                value={gender}
                disabled={false}
                placeholder={intl.formatMessage({
                  defaultMessage: 'Select Title',
                })}
                containerStyle={styles.column}
                onChange={value => setGender(value)}
              />
              <TextInput
                label={intl.formatMessage({defaultMessage: 'First Name'})}
                value={firstName}
                placeholder={intl.formatMessage({
                  defaultMessage: 'Enter First Name',
                })}
                containerStyle={styles.column}
                onChange={setFirstName}
              />
            </View>
            <TextInput
              label={intl.formatMessage({defaultMessage: 'Last Name'})}
              value={lastName}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter Last Name',
              })}
              onChange={setLastName}
            />
            <DateTimePicker
              label={intl.formatMessage({defaultMessage: 'Date of Birth'})}
              value={dob}
              placeholder={intl.formatMessage({defaultMessage: 'Select Date'})}
              onChange={value => setDob(value)}
            />
            <TextInput
              label={intl.formatMessage({defaultMessage: 'Email Address'})}
              value={email}
              right={
                <Touchable
                  style={styles.changeBtn}
                  onPress={_handleChangePress('NewEmail')}>
                  <Text fontSize={11} color={Colors.secondary}>
                    {intl.formatMessage({defaultMessage: 'Change'})}
                  </Text>
                </Touchable>
              }
              editable={false}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter Your Email',
              })}
              keyboardType="email-address"
              onChange={setEmail}
            />
            <TextInput
              label={intl.formatMessage({
                defaultMessage: 'Social Issuance Number',
              })}
              value={socialIssuanceNumber}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter Your Social Issuance Number',
              })}
              keyboardType="phone-pad"
              onChange={setSocialIssuanceNumber}
            />
            <PhoneInput
              label={intl.formatMessage({
                defaultMessage: 'Phone Number',
              })}
              value={phoneNumber}
              right={
                <Touchable
                  style={styles.changeBtn}
                  onPress={_handleChangePress('EnterPhoneNumber')}>
                  <Text fontSize={11} color={Colors.secondary}>
                    {intl.formatMessage({defaultMessage: 'Change'})}
                  </Text>
                </Touchable>
              }
              dialCode={dialCode}
              editable={false}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter Your Phone Number',
              })}
              onChange={setPhoneNumber}
              onDialCodeChange={setDialCode}
            />
            <TextInput
              label={intl.formatMessage({
                defaultMessage: 'Address',
              })}
              value={address}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter Your Address',
              })}
              onChange={setAddress}
            />
          </View>
          <Button
            style={styles.updateBtn}
            title={intl.formatMessage({defaultMessage: 'Update'})}
            loading={loading}
            disabled={disabled}
            onPress={_handleUpdatePress}
          />
          <ReAuthModal
            isVisible={reAuthVisible}
            onCancel={_toggleReAuthVisible}
            onFinish={_handleReAuthFinish}
          />
        </Content>
      </KeyboardAvoidingView>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      paddingHorizontal: 20,
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: -6,
    },
    column: {
      width: null,
      flex: 1,
      paddingHorizontal: 6,
    },
    changeBtn: {
      height: 50,
      justifyContent: 'center',
    },
    updateBtn: {
      marginTop: 20,
      marginHorizontal: 20,
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
  user: selectUser(state),
});

const mapDispatchToProps = {
  getUser: getUserAction,
  updateUser: updateUserAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(MyAccountScreen);
