import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import SettingsScreen from './SettingsScreen';
import MyAccountScreen from './MyAccountScreen';
import SecurityScreen from './SecurityScreen';
import NotificationsScreen from './NotificationsScreen';
import LanguageScreen from './LanguageScreen';
import NewEmailScreen from './NewEmailScreen';
import NewPasswordScreen from './NewPasswordScreen';
import EnterPhoneNumberScreen from './EnterPhoneNumberScreen';
import PhoneVerificationScreen from './PhoneVerificationScreen';

const Stack = createStackNavigator();

/* =============================================================================
<SettingsStack />
============================================================================= */
const SettingsStack = () => {
  return (
    <Stack.Navigator
      headerMode="none"
      screenOptions={TransitionPresets.SlideFromRightIOS}>
      <Stack.Screen name="SettingsHome" component={SettingsScreen} />
      <Stack.Screen name="MyAccount" component={MyAccountScreen} />
      <Stack.Screen name="Security" component={SecurityScreen} />
      <Stack.Screen name="Notifications" component={NotificationsScreen} />
      <Stack.Screen name="Language" component={LanguageScreen} />
      <Stack.Screen name="NewEmail" component={NewEmailScreen} />
      <Stack.Screen name="NewPassword" component={NewPasswordScreen} />
      <Stack.Screen
        name="EnterPhoneNumber"
        component={EnterPhoneNumberScreen}
      />
      <Stack.Screen
        name="PhoneVerification"
        component={PhoneVerificationScreen}
      />
    </Stack.Navigator>
  );
};

/* Export
============================================================================= */
export default SettingsStack;
