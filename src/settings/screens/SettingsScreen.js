import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import SettingsLink from '../components/SettingsLink';
import {Content, Container, ModalHeader, View, Button} from '../../common';
import * as Colors from '../../config/colors';

import UserIcon from '../../assets/icons/edit-profile.svg';
import ShieldIcon from '../../assets/icons/edit-security.svg';
import BellIcon from '../../assets/icons/edit-bell.svg';
import LanguageIcon from '../../assets/icons/edit-language.svg';
import PageIcon from '../../assets/icons/edit-page.svg';
import MobileIcon from '../../assets/icons/edit-mobile.svg';
import {logout as logoutAction} from '../../auth/redux/actions';

/* =============================================================================
<SettingsScreen />
============================================================================= */
const SettingsScreen = ({logout}) => {
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);

  const _handleLogoutPress = () => {
    logout();
  };

  return (
    <Container>
      <ModalHeader
        title={<FormattedMessage defaultMessage="Advanced Settings" />}
      />
      <Content>
        <View flex={1} paddingHorizontal={20}>
          <SettingsLink
            to="MyAccount"
            icon={<UserIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="My Account" />}
          />
          <SettingsLink
            to="Security"
            icon={<ShieldIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Security" />}
          />
          <SettingsLink
            to="Notifications"
            icon={<BellIcon fill={Colors.text} />}
            title={
              <FormattedMessage defaultMessage="Customize Notifications" />
            }
          />
          <SettingsLink
            to="Language"
            icon={<LanguageIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Language" />}
          />
          <SettingsLink
            to="PrivacyPolicy"
            icon={<PageIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="Privacy Policy" />}
          />
          <SettingsLink
            to="AppVersion"
            icon={<MobileIcon fill={Colors.text} />}
            title={<FormattedMessage defaultMessage="App Version" />}
          />
        </View>
        <Button
          style={styles.logoutBtn}
          textStyle={styles.logoutBtnTxt}
          onPress={_handleLogoutPress}
          title={<FormattedMessage defaultMessage="Logout" />}
        />
      </Content>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    logoutBtn: {
      marginHorizontal: 20,
      marginBottom: insets.bottom || 20,
      borderWidth: 0.5,
      borderColor: Colors.danger,
      backgroundColor: Colors.background,
    },
    logoutBtnTxt: {
      color: Colors.danger,
    },
  });

const mapDispatchToProps = {
  logout: logoutAction,
};

/* Export
============================================================================= */
export default connect(null, mapDispatchToProps)(SettingsScreen);
