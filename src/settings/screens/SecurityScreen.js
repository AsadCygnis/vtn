import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {useIntl} from 'react-intl';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {
  Content,
  Container,
  StackHeader,
  View,
  Button,
  TextInput,
  SwitchInput,
  Touchable,
  Text,
} from '../../common';
import * as Colors from '../../config/colors';

import {
  getLoading,
  getSecurity as selectSecurity,
} from '../../auth/redux/selectors';
import {
  getSecurity as getSecurityAction,
  updateSecurity as updateSecurityAction,
} from '../../auth/redux/actions';

/* =============================================================================
<SecurityScreen />
============================================================================= */
const SecurityScreen = ({loading, security, getSecurity, updateSecurity}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const styles = getStyles(insets);
  const [faceId, setFaceId] = useState(false);
  const [fingerprint, setFingerprint] = useState(false);
  const [notifyEmailLogin, setNotifyEmaiLogin] = useState(false);
  const [twoFactorAuthentication, setTwoFactorAuthentication] = useState(false);
  const [locationBasedSecurity, setLocationBasedSecurity] = useState(false);
  const [contactlessPayment, setContactlessPayment] = useState(false);
  const disabled = loading;

  // Get security and user info
  useEffect(() => {
    getSecurity();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // Sync with form values
  useEffect(() => {
    if (security) {
      setFaceId(!!security.faceId);
      setFingerprint(!!security.fingerprint);
      setNotifyEmaiLogin(!!security.notifyEmailLogin);
      setTwoFactorAuthentication(!!security.twoFactorAuthentication);
      setLocationBasedSecurity(!!security.locationBasedSecurity);
      setContactlessPayment(!!security.contactlessPayment);
    }
  }, [security]);

  const _handlePasswordChangePress = () => {
    navigation.navigate('NewPassword');
  };

  const _handleUpdatePress = () => {
    if (!disabled) {
      updateSecurity(
        {
          faceId,
          fingerprint,
          notifyEmailLogin,
          twoFactorAuthentication,
          locationBasedSecurity,
          contactlessPayment,
        },
        () => {
          Alert.alert(
            intl.formatMessage({defaultMessage: 'Success'}),
            intl.formatMessage({
              defaultMessage: 'Settings updated successfully.',
            }),
          );
        },
      );
    }
  };

  return (
    <Container>
      <StackHeader title={intl.formatMessage({defaultMessage: 'Security'})} />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.select({ios: 'padding', android: undefined})}>
        <Content>
          <View style={styles.form}>
            <SwitchInput
              title={intl.formatMessage({defaultMessage: 'Face ID'})}
              value={faceId}
              onChange={setFaceId}
            />
            <SwitchInput
              title={intl.formatMessage({defaultMessage: 'Fingerprint'})}
              value={fingerprint}
              onChange={setFingerprint}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Notify Email Every Login',
              })}
              value={notifyEmailLogin}
              onChange={setNotifyEmaiLogin}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: '2 Factor Authentication',
              })}
              value={twoFactorAuthentication}
              onChange={setTwoFactorAuthentication}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Location Based Security',
              })}
              value={locationBasedSecurity}
              onChange={setLocationBasedSecurity}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Contactless Payments',
              })}
              value={contactlessPayment}
              onChange={setContactlessPayment}
            />
            <TextInput
              label={intl.formatMessage({defaultMessage: 'Password'})}
              value="***************"
              right={
                <Touchable
                  style={styles.passwordChangeBtn}
                  onPress={_handlePasswordChangePress}>
                  <Text color={Colors.primary}>Change</Text>
                </Touchable>
              }
              editable={false}
              secureTextEntry
            />
          </View>
          <Button
            style={styles.updateBtn}
            title={intl.formatMessage({defaultMessage: 'Update'})}
            loading={loading}
            disabled={disabled}
            onPress={_handleUpdatePress}
          />
        </Content>
      </KeyboardAvoidingView>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      paddingHorizontal: 20,
    },
    passwordChangeBtn: {
      height: 50,
      alignItems: 'center',
      justifyContent: 'center',
    },
    updateBtn: {
      marginTop: 20,
      marginHorizontal: 20,
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
  security: selectSecurity(state),
});

const mapDispatchToProps = {
  getSecurity: getSecurityAction,
  updateSecurity: updateSecurityAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(SecurityScreen);
