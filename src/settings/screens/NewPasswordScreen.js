import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useIntl} from 'react-intl';
import {useNavigation} from '@react-navigation/core';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {
  View,
  Button,
  TextInput,
  Content,
  ModalHeader,
  Container,
} from '../../common';

import {getLoading} from '../../auth/redux/selectors';
import {
  resetPassword as resetPasswordAction,
  reAuthenticate as reAuthenticateAction,
} from '../../auth/redux/actions';

/* =============================================================================
<NewPasswordScreen />
============================================================================= */
const NewPasswordScreen = ({loading, resetPassword, reAuthenticate}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [oldPassword, setOldPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const disabled = !oldPassword || !password || !confirmPassword || loading;
  const styles = getStyles(insets);

  const _handleSubmit = () => {
    if (!disabled) {
      if (password === confirmPassword) {
        reAuthenticate(oldPassword, () => {
          resetPassword(password, () => {
            Alert.alert(
              intl.formatMessage({defaultMessage: 'Success'}),
              intl.formatMessage({
                defaultMessage: 'Password updated successfully',
              }),
              [
                {
                  text: intl.formatMessage({defaultMessage: 'Ok'}),
                  onPress: () => {
                    navigation.goBack();
                  },
                },
              ],
              {
                cancelable: false,
              },
            );
          });
        });
      } else {
        Alert.alert(
          intl.formatMessage({defaultMessage: 'Error'}),
          intl.formatMessage({defaultMessage: 'Password did not match'}),
        );
      }
    }
  };

  return (
    <Container>
      <ModalHeader
        title={intl.formatMessage({defaultMessage: 'Setup New Password'})}
      />
      <KeyboardAvoidingView
        style={styles.keyboardView}
        behavior={Platform.select({ios: 'padding', android: undefined})}>
        <Content>
          <View style={styles.form}>
            <TextInput
              label={intl.formatMessage({
                defaultMessage: 'Old Password',
              })}
              value={oldPassword}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter Old Password',
              })}
              secureTextEntry
              onChange={setOldPassword}
            />
            <TextInput
              label={intl.formatMessage({
                defaultMessage: 'New Password',
              })}
              value={password}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter New Password',
              })}
              secureTextEntry
              onChange={setPassword}
            />
            <TextInput
              label={intl.formatMessage({
                defaultMessage: 'Confirm Password',
              })}
              value={confirmPassword}
              placeholder={intl.formatMessage({
                defaultMessage: 'Re-enter New Password',
              })}
              secureTextEntry
              onChange={setConfirmPassword}
            />
          </View>
          <Button
            title={intl.formatMessage({defaultMessage: 'Save New Password'})}
            style={styles.submitBtn}
            loading={loading}
            disabled={disabled}
            onPress={_handleSubmit}
          />
        </Content>
      </KeyboardAvoidingView>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    keyboardView: {
      flex: 1,
    },
    form: {
      flex: 1,
      paddingHorizontal: 20,
    },
    submitBtn: {
      marginTop: 20,
      marginHorizontal: 20,
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
});

const mapDispatchToProps = {
  resetPassword: resetPasswordAction,
  reAuthenticate: reAuthenticateAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(NewPasswordScreen);
