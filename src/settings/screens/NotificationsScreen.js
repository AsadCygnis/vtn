import React, {useEffect, useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {
  Content,
  Container,
  StackHeader,
  View,
  Text,
  Button,
  SwitchInput,
} from '../../common';
import * as Colors from '../../config/colors';

import {
  getLoading,
  getNotifications as selectNotifications,
} from '../redux/selectors';
import {
  getNotifications as getAppSettingsAction,
  updateNotifications as updateNotificationsAction,
} from '../redux/actions';

/* =============================================================================
<NotificationsScreen />
============================================================================= */
const NotificationsScreen = ({
  loading,
  notifications,
  getNotifications,
  updateNotifications,
}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);
  const [transactionsSent, setTransactionsSent] = useState(false);
  const [transactionsReceived, setTransactionsReceived] = useState(false);
  const [splitBill, setSplitBill] = useState(false);
  const [cardUsed, setCardUsed] = useState(false);
  const [suspiciousActivity, setSuspiciousActivity] = useState(false);
  const [limitApproach, setLimitApproach] = useState(false);
  const [newCurrencyAvailable, setNewCurrencyAvailable] = useState(false);
  const [smartContractActivity, setSmartContractActivity] = useState(false);
  const [billReminder, setBillReminder] = useState(false);
  const disabled = loading;

  useEffect(() => {
    getNotifications();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (notifications) {
      setTransactionsSent(!!notifications.transactionsSent);
      setTransactionsReceived(!!notifications.transactionsReceived);
      setSplitBill(!!notifications.splitBill);
      setCardUsed(!!notifications.cardUsed);
      setSuspiciousActivity(!!notifications.suspiciousActivity);
      setLimitApproach(!!notifications.limitApproach);
      setNewCurrencyAvailable(!!notifications.newCurrencyAvailable);
      setSmartContractActivity(!!notifications.smartContractActivity);
      setBillReminder(!!notifications.billReminder);
    }
  }, [notifications]);

  const _handleUpdatePress = () => {
    if (!disabled) {
      updateNotifications(
        {
          transactionsSent,
          transactionsReceived,
          splitBill,
          cardUsed,
          suspiciousActivity,
          limitApproach,
          newCurrencyAvailable,
          smartContractActivity,
          billReminder,
        },
        () => {
          Alert.alert(
            intl.formatMessage({defaultMessage: 'Success'}),
            intl.formatMessage({
              defaultMessage: 'Settings updated successfully.',
            }),
          );
        },
      );
    }
  };

  return (
    <Container>
      <StackHeader
        title={intl.formatMessage({defaultMessage: 'Notifications'})}
      />
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.select({ios: 'padding', android: undefined})}>
        <Content>
          <View style={styles.form}>
            <Text style={styles.label}>
              {intl.formatMessage({defaultMessage: 'Transactions'})}
            </Text>
            <SwitchInput
              title={intl.formatMessage({defaultMessage: 'Sent'})}
              value={transactionsSent}
              containerStyle={styles.input}
              onChange={setTransactionsSent}
            />
            <SwitchInput
              title={intl.formatMessage({defaultMessage: 'Received'})}
              value={transactionsReceived}
              containerStyle={styles.input}
              onChange={setTransactionsReceived}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Split Bill',
              })}
              value={splitBill}
              containerStyle={styles.input}
              onChange={setSplitBill}
            />
            <View style={styles.hr} />
            <Text style={styles.label}>
              {intl.formatMessage({defaultMessage: 'Card'})}
            </Text>
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Card Used',
              })}
              value={cardUsed}
              containerStyle={styles.input}
              onChange={setCardUsed}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Suspicious Activity',
              })}
              value={suspiciousActivity}
              containerStyle={styles.input}
              onChange={setSuspiciousActivity}
            />
            <View style={styles.hr} />
            <Text style={styles.label}>
              {intl.formatMessage({defaultMessage: 'Account'})}
            </Text>
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Limit Approach',
              })}
              value={limitApproach}
              containerStyle={styles.input}
              onChange={setLimitApproach}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'New Currency Available',
              })}
              value={newCurrencyAvailable}
              containerStyle={styles.input}
              onChange={setNewCurrencyAvailable}
            />
            <View style={styles.hr} />
            <Text style={styles.label}>
              {intl.formatMessage({defaultMessage: 'General'})}
            </Text>
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Smart Contract Activity',
              })}
              value={smartContractActivity}
              containerStyle={styles.input}
              onChange={setSmartContractActivity}
            />
            <SwitchInput
              title={intl.formatMessage({
                defaultMessage: 'Bill Reminder',
              })}
              value={billReminder}
              containerStyle={styles.input}
              onChange={setBillReminder}
            />
          </View>
          <Button
            style={styles.updateBtn}
            title={intl.formatMessage({defaultMessage: 'Update'})}
            loading={loading}
            disabled={disabled}
            onPress={_handleUpdatePress}
          />
        </Content>
      </KeyboardAvoidingView>
    </Container>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
    },
    label: {
      marginTop: 12,
      marginHorizontal: 20,
      color: Colors.label,
      fontSize: 10,
    },
    input: {
      paddingHorizontal: 20,
    },
    hr: {
      width: '100%',
      height: 1,
      marginVertical: 12,
      backgroundColor: Colors.placeholder,
    },
    updateBtn: {
      marginTop: 20,
      marginHorizontal: 20,
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
  notifications: selectNotifications(state),
});

const mapDispatchToProps = {
  getNotifications: getAppSettingsAction,
  updateNotifications: updateNotificationsAction,
};

/* Export
============================================================================= */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NotificationsScreen);
