/**
 * Get user
 */
export const getUser = state => state.Settings.user;

/**
 * Get error
 */
export const getError = state => state.Settings.error;

/**
 * Get loading
 */
export const getLoading = state => state.Settings.loading;

/**
 * Get app settings
 */
export const getAppSettings = state => state.Settings.appSettings;

/**
 * Get notifications
 */
export const getNotifications = state => state.Settings.notifications;
