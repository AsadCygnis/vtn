import {actionGenerator} from '../../util/reduxHelpers';

export const USER_GET = actionGenerator('SETTINGS/USER_GET');
export const USER_UPDATE = actionGenerator('SETTINGS/USER_UPDATE');
export const USER_AVATAR_UPDATE = actionGenerator(
  'SETTINGS/USER_AVATAR_UPDATE',
);
export const NOTIFICATIONS_GET = actionGenerator('SETTINGS/NOTIFICATIONS_GET');
export const NOTIFICATIONS_UPDATE = actionGenerator(
  'SETTINGS/NOTIFICATIONS_UPDATE',
);
export const APP_SETTINGS_GET = actionGenerator('SETTINGS/APP_SETTINGS_GET');
export const APP_SETTINGS_UPDATE = actionGenerator(
  'SETTINGS/APP_SETTINGS_UPDATE',
);
