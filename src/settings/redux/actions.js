import {Alert} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import firebaseAuth from '@react-native-firebase/auth';
import firebaseStorage from '@react-native-firebase/storage';

import * as constants from './constants';
import {updateProfile} from '../../auth/redux/actions';
import {UsersCollection} from '../../config/firebase';

/**
 * USER_GET
 */
export const getUser = () => async dispatch => {
  try {
    dispatch({type: constants.USER_GET.REQUEST});

    const user = firebaseAuth().currentUser;
    if (!user) {
      throw new Error('Unauthorized');
    }

    const userSnapshot = await UsersCollection.doc(user.uid).get();
    if (userSnapshot.exists) {
      const payload = {
        ...userSnapshot.data(),
        id: user.uid,
      };
      dispatch({
        type: constants.USER_GET.SUCCESS,
        payload,
      });
    }
  } catch (error) {
    dispatch({type: constants.USER_GET.FAIL, error});
  } finally {
    dispatch({type: constants.USER_GET.COMPLETE});
  }
};

/**
 * USER_UPDATE
 */
export const updateUser = (update, cb) => async dispatch => {
  try {
    dispatch({type: constants.USER_UPDATE.REQUEST});

    const id = firebaseAuth().currentUser && firebaseAuth().currentUser.uid;

    await UsersCollection.doc(id).update({
      ...update,
      updatedAt: firestore.FieldValue.serverTimestamp(),
    });

    if (update.firstName && update.lastName) {
      dispatch(
        updateProfile({displayName: `${update.firstName} ${update.lastName}`}),
      );
    }
    dispatch({
      type: constants.USER_UPDATE.SUCCESS,
      payload: update,
    });
    if (typeof cb === 'function') {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.USER_UPDATE.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('', message);
  } finally {
    dispatch({type: constants.USER_UPDATE.COMPLETE});
  }
};

/**
 * USER_AVATAR_UPDATE
 */
export const updateUserAvatar = (file, cb) => async dispatch => {
  try {
    dispatch({type: constants.USER_AVATAR_UPDATE.REQUEST});

    const user = firebaseAuth().currentUser.uid;
    const imagePath = `images/avatar/${user}/${file.fileName}`;
    const filePath = file.uri;
    await firebaseStorage().ref(imagePath).putFile(filePath);

    const avatar = await firebaseStorage().ref(imagePath).getDownloadURL();
    await UsersCollection.doc(user).update({
      avatar,
      updatedAt: firestore.FieldValue.serverTimestamp(),
    });

    dispatch(updateProfile({photoURL: avatar}));
    dispatch({
      type: constants.USER_AVATAR_UPDATE.SUCCESS,
      payload: avatar,
    });
    if (typeof cb === 'function') {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.USER_AVATAR_UPDATE.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('', message);
  } finally {
    dispatch({type: constants.USER_AVATAR_UPDATE.COMPLETE});
  }
};

/**
 * NOTIFICATIONS_GET
 */
export const getNotifications = () => async dispatch => {
  try {
    dispatch({type: constants.NOTIFICATIONS_GET.REQUEST});

    const id = firebaseAuth().currentUser && firebaseAuth().currentUser.uid;

    const notificationsSnapshot = await UsersCollection.doc(id)
      .collection('settings')
      .doc('notifications')
      .get();

    if (notificationsSnapshot.exists) {
      const {tokens, ...notifications} = notificationsSnapshot.data();
      dispatch({
        type: constants.NOTIFICATIONS_GET.SUCCESS,
        payload: notifications,
      });
    }
  } catch (error) {
    dispatch({type: constants.NOTIFICATIONS_GET.FAIL, error});
  } finally {
    dispatch({type: constants.NOTIFICATIONS_GET.COMPLETE});
  }
};

/**
 * NOTIFICATIONS_UPDATE
 */
export const updateNotifications = (notifications, cb) => async dispatch => {
  try {
    dispatch({type: constants.NOTIFICATIONS_UPDATE.REQUEST});

    const id = firebaseAuth().currentUser && firebaseAuth().currentUser.uid;

    await UsersCollection.doc(id)
      .collection('settings')
      .doc('notifications')
      .update(notifications);

    dispatch({
      type: constants.NOTIFICATIONS_UPDATE.SUCCESS,
      payload: notifications,
    });
    if (typeof cb === 'function') {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.NOTIFICATIONS_UPDATE.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('Error', message);
  } finally {
    dispatch({type: constants.NOTIFICATIONS_UPDATE.COMPLETE});
  }
};

/**
 * APP_SETTINGS_GET
 */
export const getAppSettings = () => async dispatch => {
  try {
    dispatch({type: constants.APP_SETTINGS_GET.REQUEST});

    const id = firebaseAuth().currentUser && firebaseAuth().currentUser.uid;

    const appSettingsSnapshot = await UsersCollection.doc(id)
      .collection('settings')
      .doc('app')
      .get();

    if (appSettingsSnapshot.exists) {
      dispatch({
        type: constants.APP_SETTINGS_GET.SUCCESS,
        payload: appSettingsSnapshot.data(),
      });
    }
  } catch (error) {
    dispatch({type: constants.APP_SETTINGS_GET.FAIL, error});
  } finally {
    dispatch({type: constants.APP_SETTINGS_GET.COMPLETE});
  }
};

/**
 * APP_SETTINGS_UPDATE
 */
export const updateAppSettings = (update, cb) => async dispatch => {
  try {
    dispatch({type: constants.APP_SETTINGS_UPDATE.REQUEST});

    const id = firebaseAuth().currentUser && firebaseAuth().currentUser.uid;

    await UsersCollection.doc(id)
      .collection('settings')
      .doc('app')
      .update(update);

    dispatch({
      type: constants.APP_SETTINGS_UPDATE.SUCCESS,
      payload: update,
    });
    if (typeof cb === 'function') {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.APP_SETTINGS_UPDATE.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('Error', message);
  } finally {
    dispatch({type: constants.APP_SETTINGS_UPDATE.COMPLETE});
  }
};
