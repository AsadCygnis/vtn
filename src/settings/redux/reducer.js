import * as constants from './constants';

export const INITIAL_STATE = {
  user: {},
  error: null,
  loading: false,
  appSettings: {},
  notifications: {},
};

export default function reducer(state = INITIAL_STATE, action) {
  const {type, payload} = action;

  switch (type) {
    // USER_GET
    case constants.USER_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.USER_GET.FAIL:
      return {
        ...state,
        error: payload,
      };
    case constants.USER_GET.SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          ...payload,
        },
      };
    case constants.USER_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // USER_UPDATE
    case constants.USER_UPDATE.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.USER_UPDATE.FAIL:
      return {
        ...state,
        error: payload,
      };
    case constants.USER_UPDATE.SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          ...payload,
        },
      };
    case constants.USER_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // USER_AVATAR_UPDATE
    case constants.USER_AVATAR_UPDATE.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.USER_AVATAR_UPDATE.FAIL:
      return {
        ...state,
        error: payload,
      };
    case constants.USER_AVATAR_UPDATE.SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          avatar: payload,
        },
      };
    case constants.USER_AVATAR_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // NOTIFICATIONS_GET
    case constants.NOTIFICATIONS_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.NOTIFICATIONS_GET.FAIL:
      return {
        ...state,
        error: payload,
      };
    case constants.NOTIFICATIONS_GET.SUCCESS:
      return {
        ...state,
        notifications: payload,
      };
    case constants.NOTIFICATIONS_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // NOTIFICATIONS_UPDATE
    case constants.NOTIFICATIONS_UPDATE.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.NOTIFICATIONS_UPDATE.FAIL:
      return {
        ...state,
        error: payload,
      };
    case constants.NOTIFICATIONS_UPDATE.SUCCESS:
      return {
        ...state,
        notifications: {
          ...state.notifications,
          ...payload,
        },
      };
    case constants.NOTIFICATIONS_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // APP_SETTINGS_GET
    case constants.APP_SETTINGS_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.APP_SETTINGS_GET.FAIL:
      return {
        ...state,
        error: payload,
      };
    case constants.APP_SETTINGS_GET.SUCCESS:
      return {
        ...state,
        appSettings: {
          ...state.appSettings,
          ...payload,
        },
      };
    case constants.APP_SETTINGS_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // APP_SETTINGS_UPDATE
    case constants.APP_SETTINGS_UPDATE.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.APP_SETTINGS_UPDATE.FAIL:
      return {
        ...state,
        error: payload,
      };
    case constants.APP_SETTINGS_UPDATE.SUCCESS:
      return {
        ...state,
        appSettings: {
          ...state.appSettings,
          ...payload,
        },
      };
    case constants.APP_SETTINGS_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
