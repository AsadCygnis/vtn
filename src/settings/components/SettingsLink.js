import React from 'react';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {Text, View, Touchable} from '../../common';
import * as Colors from '../../config/colors';

/* =============================================================================
<SettingsLink />
============================================================================= */
const SettingsLink = ({to, icon, title}) => {
  const navigation = useNavigation();

  const _handlePress = () => {
    navigation.navigate(to);
  };

  return (
    <Touchable style={styles.container} onPress={_handlePress}>
      <View style={styles.iconContainer}>{icon}</View>
      <Text style={styles.title}>{title}</Text>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  iconContainer: {
    width: 44,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    backgroundColor: Colors.card,
  },
  title: {
    flex: 1,
    marginLeft: 12,
    fontFamily: 'Poppins-SemiBold',
  },
});

/* Export
============================================================================= */
export default SettingsLink;
