import React from 'react';
import {StyleSheet} from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

import {Text, Touchable} from '../../common';
import * as Colors from '../../config/colors';

/* =============================================================================
<LanguageItem />
============================================================================= */
const LanguageItem = ({title, name, value, onPress}) => {
  return (
    <Touchable style={styles.container} onPress={onPress}>
      <Text style={styles.title}>{title}</Text>
      {value === name && (
        <FontAwesome5Icon
          name="check-square"
          color={Colors.primary}
          size={16}
          solid
        />
      )}
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 12,
    marginBottom: 8,
    paddingBottom: 12,
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.placeholder,
  },
  title: {
    fontFamily: 'Poppins-Medium',
  },
});

export default LanguageItem;
