import React from 'react';

import {Container} from '../../common';
import ProfileHeader from '../components/ProfileHeader';
import ProfileContactList from '../components/ProfileContactList';

/* =============================================================================
<ProfileScreen />
============================================================================= */
const ProfileScreen = () => {
  return (
    <Container>
      <ProfileHeader />
      <ProfileContactList />
    </Container>
  );
};

/* Export
============================================================================= */
export default ProfileScreen;
