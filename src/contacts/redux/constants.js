import {actionGenerator} from '../../util/reduxHelpers';

export const CONTACT_GET = actionGenerator('CONTACTS/CONTACT_GET');
export const CONTACTS_GET = actionGenerator('CONTACTS/CONTACTS_GET');
