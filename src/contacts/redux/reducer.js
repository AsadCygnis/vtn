import * as constants from './constants';
import * as helpers from '../../util/reduxHelpers';

export const INITIAL_STATE = {
  byId: {},
  allIds: [],
  error: null,
  loading: false,
};

export default function reducer(state = INITIAL_STATE, action) {
  const {type, payload, error} = action;

  switch (type) {
    // CONTACT_GET
    case constants.CONTACT_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.CONTACT_GET.FAIL:
      return {
        ...state,
        error,
      };
    case constants.CONTACT_GET.SUCCESS:
      return helpers.add(state, payload);
    case constants.CONTACT_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // CONTACTS_GET
    case constants.CONTACTS_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.CONTACTS_GET.FAIL:
      return {
        ...state,
        error,
      };
    case constants.CONTACTS_GET.SUCCESS:
      return helpers.merge(state, payload);
    case constants.CONTACTS_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
