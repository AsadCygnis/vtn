import firebaseAuth from '@react-native-firebase/auth';

import * as constants from './constants';
import {ContactsCollection} from '../../config/firebase';

/**
 * CONTACT_GET
 */
export const getContact = id => async dispatch => {
  try {
    dispatch({type: constants.CONTACT_GET.REQUEST});

    const contactSnapshot = await ContactsCollection.doc(id).get();

    if (contactSnapshot.exists) {
      dispatch({
        type: constants.CONTACT_GET.SUCCESS,
        payload: {
          id: contactSnapshot.id,
          ...contactSnapshot.data(),
        },
      });
    }
  } catch (error) {
    dispatch({type: constants.CONTACT_GET.FAIL, error});
  } finally {
    dispatch({type: constants.CONTACT_GET.COMPLETE});
  }
};

/**
 * CONTACTS_GET
 */
export const getContacts = () => async dispatch => {
  try {
    dispatch({type: constants.CONTACTS_GET.REQUEST});

    const uid = firebaseAuth().currentUser.uid;
    const contactsSnapshot = await ContactsCollection.where(
      'user',
      '==',
      uid,
    ).get();

    const contacts = [];
    contactsSnapshot.forEach(snapshot => {
      if (snapshot.exists) {
        contacts.push({
          id: snapshot.id,
          ...snapshot.data(),
        });
      }
    });

    dispatch({
      type: constants.CONTACTS_GET.SUCCESS,
      payload: contacts,
    });
  } catch (error) {
    dispatch({type: constants.CONTACTS_GET.FAIL, error});
  } finally {
    dispatch({type: constants.CONTACTS_GET.COMPLETE});
  }
};
