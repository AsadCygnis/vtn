import {createSelector} from 'reselect';
// import {firestoreTimestampToDate} from '../../util/functions';

/**
 * Get error
 */
export const getError = state => state.Contacts.error;

/**
 * Get loading
 */
export const getLoading = state => state.Contacts.loading;

/**
 * Get all ids
 */
export const getAllIds = state => state.Contacts.allIds;

/**
 * Get byId
 */
export const getById = state => state.Contacts.byId;

/**
 * Get contact ids
 */
export const getContactIds = createSelector(
  [getAllIds, getById],
  (allIds, byId) =>
    allIds.slice().sort((a, b) => byId[a].createdAt - byId[b].createdAt),
);

/**
 * Get id
 */
const getId = (state, {id}) => id;

/**
 * Get contact by id
 */
export const makeGetContactById = () =>
  createSelector([getById, getId], (byId, id) => ({
    ...byId[id],
    // createdAt: byId[id].createdAt,
  }));
