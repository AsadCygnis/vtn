import React, {useState} from 'react';
import {connect} from 'react-redux';
// import {useNavigation} from '@react-navigation/native';
import {FormattedMessage} from 'react-intl';
import {Image, StyleSheet} from 'react-native';

import {Text, View, Touchable, ComingSoonModal} from '../../common';
import * as Colors from '../../config/colors';

import BasicAccountImage from '../../assets/images/account-badge-basic.png';
import GoldAccountImage from '../../assets/images/account-badge-gold.png';
import VipAccountImage from '../../assets/images/account-badge-vip.png';

/* =============================================================================
<ProfileAccountType />
============================================================================= */
const ProfileAccountType = ({accountType}) => {
  // const navigation = useNavigation();
  const [comingSoonModal, setComingSoonModal] = useState(false);

  const _toggleComingSoonModal = () => {
    setComingSoonModal(prevState => !prevState);
  };

  const _handlePress = () => {
    _toggleComingSoonModal();
    // navigation.navigate('UpgradeAccount');
  };

  return (
    <Touchable style={styles.container} onPress={_handlePress}>
      {accountType === 'basic' && (
        <Image source={BasicAccountImage} style={styles.badge} />
      )}
      {accountType === 'gold' && (
        <Image source={GoldAccountImage} style={styles.badge} />
      )}
      {accountType === 'vip' && (
        <Image source={VipAccountImage} style={styles.badge} />
      )}
      <View flex={1} paddingHorizontal={12}>
        <Text style={styles.title}>
          <FormattedMessage
            defaultMessage="{accountType} Account"
            values={{accountType}}
          />
        </Text>
        <Text style={styles.description}>
          {accountType === 'basic' && (
            <FormattedMessage defaultMessage="Limited Features - Tap to Upgrade" />
          )}
          {accountType === 'gold' && (
            <FormattedMessage defaultMessage="Standard Features - Tap to Upgrade" />
          )}
          {accountType === 'vip' && (
            <FormattedMessage defaultMessage="Full features has been unlocked." />
          )}
        </Text>
      </View>
      <ComingSoonModal
        visible={comingSoonModal}
        onCancel={_toggleComingSoonModal}
      />
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    paddingVertical: 16,
    borderWidth: 1,
    borderColor: Colors.placeholder,
    borderRadius: 4,
    backgroundColor: Colors.card,
  },
  badge: {
    width: 50,
    height: 64,
  },
  title: {
    fontSize: 16,
    fontFamily: 'Poppins-SemiBold',
    textTransform: 'capitalize',
  },
  description: {
    marginTop: 3,
    color: Colors.label,
  },
});

const mapStateToProps = state => ({
  accountType: 'basic',
});

/* Export
============================================================================= */
export default connect(mapStateToProps)(ProfileAccountType);
