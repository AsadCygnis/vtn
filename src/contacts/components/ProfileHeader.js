import React from 'react';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/core';
import {Image, StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Text, View, Touchable, Avatar} from '../../common';
import BackgroundImage from '../../assets/images/profile-background.png';
import CloseIcon from '../../assets/icons/nav-close.svg';
import * as Colors from '../../config/colors';

import {getUser} from '../../settings/redux/selectors';

/* =============================================================================
<ProfileHeader />
============================================================================= */
const ProfileHeader = ({user}) => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const avatar = user ? user.photoURL : '';
  const username = user ? `${user.firstName} ${user.lastName}` : '';
  const address = user ? user.address : '';
  const backgroundStyle = {
    height: insets.top + 157,
  };
  const closeBtnStyle = {
    top: insets.top,
  };

  const _handleClosePress = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <Image
        source={BackgroundImage}
        style={[styles.background, backgroundStyle]}
      />
      <Touchable
        style={[styles.closeBtn, closeBtnStyle]}
        onPress={_handleClosePress}>
        <CloseIcon />
      </Touchable>
      <Avatar uri={avatar} size={80} radius={8} style={styles.avatar} />
      <Text style={styles.username}>{username} </Text>
      <Text style={styles.address}>{address} </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingBottom: 12,
    backgroundColor: 'transparent',
  },
  background: {
    width: '100%',
    resizeMode: 'stretch',
  },
  closeBtn: {
    position: 'absolute',
    left: 0,
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  avatar: {
    marginTop: -40,
  },
  username: {
    marginTop: 8,
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Poppins-SemiBold',
  },
  address: {
    marginTop: 3,
    color: Colors.label,
    textAlign: 'center',
  },
});

const mapStateToProps = state => ({
  user: getUser(state),
});

/* Export
============================================================================= */
export default connect(mapStateToProps)(ProfileHeader);
