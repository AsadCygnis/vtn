import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {StyleSheet, FlatList} from 'react-native';

import {ListEmpty} from '../../../common';
import ProfileAccountType from '../ProfileAccountType';
import ProfileContactListItem from './ProfileContactListItem';
import ProfileContactListHeader from './ProfileContactListHeader';

import {getContactIds} from '../../redux/selectors';
// import {getContacts as getContactsAction} from '../../redux/actions';

/* =============================================================================
<ProfileContactList />
============================================================================= */
const ProfileContactList = ({contacts, getContacts}) => {
  // useEffect(() => {
  //   getContacts();
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  return (
    <FlatList
      data={contacts}
      contentContainerStyle={styles.contentContainerStyle}
      renderItem={renderItem}
      keyExtractor={renderKeyExtractor}
      ListEmptyComponent={renderListEmpty}
      ListHeaderComponent={renderListHeader}
    />
  );
};

const renderItem = ({item}) => <ProfileContactListItem id={item} />;
const renderListEmpty = () => (
  <ListEmpty title={<FormattedMessage defaultMessage="No Data" />} />
);
const renderKeyExtractor = item => `${item}`;
const renderListHeader = () => (
  <>
    <ProfileAccountType />
    <ProfileContactListHeader />
  </>
);

const styles = StyleSheet.create({
  contentContainerStyle: {
    flexGrow: 1,
    paddingBottom: 40,
    paddingHorizontal: 20,
  },
});

const mapStateToProps = state => ({
  contacts: getContactIds(state),
});

const mapDispatchToProps = {
  // getContacts: getContactsAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(ProfileContactList);
