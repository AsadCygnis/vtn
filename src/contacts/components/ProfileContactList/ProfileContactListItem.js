import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {Avatar, Text, Touchable, View} from '../../../common';
import * as Colors from '../../../config/colors';

import {makeGetContactById} from '../../redux/selectors';

/* =============================================================================
<ProfileContactListItem />
============================================================================= */
const ProfileContactListItem = ({contact}) => {
  const navigation = useNavigation();
  const avatar = contact ? contact.avatar : '';
  const username = contact ? `${contact.firstName} ${contact.lastName}` : '';
  const address = contact ? `${contact.address}` : '';

  const _handlePress = () => {
    navigation.navigate('ContactDetails', {id: contact.id});
  };

  return (
    <Touchable style={styles.container} onPress={_handlePress}>
      <Avatar uri={avatar} size={44} radius={8} />
      <View flex={1}>
        <Text style={styles.address}>{address}</Text>
        <Text style={styles.username}>{username}</Text>
      </View>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 12,
  },
  avatar: {
    width: 44,
    height: 44,
    borderRadius: 8,
    backgroundColor: Colors.card,
  },
  address: {
    marginLeft: 12,
    color: Colors.label,
    fontSize: 10,
  },
  username: {
    marginTop: 3,
    marginLeft: 12,
    fontFamily: 'Poppins-SemiBold',
  },
});

const mapStateToProps = () => {
  const getContactById = makeGetContactById();
  return (state, {id}) => ({
    contact: getContactById(state, {id}),
  });
};

export default connect(mapStateToProps)(ProfileContactListItem);
