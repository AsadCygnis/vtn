import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';

import {
  Text,
  View,
  Touchable,
  LinkButton,
  ComingSoonModal,
} from '../../../common';
import PlusIcon from '../../../assets/icons/edit-plus.svg';
import * as Colors from '../../../config/colors';

/* =============================================================================
<ProfileContactListHeader />
============================================================================= */
const ProfileContactListHeader = () => {
  const [comingSoonModal, setComingSoonModal] = useState(false);

  const _toggleComingSoonModal = () => {
    setComingSoonModal(prevState => !prevState);
  };

  const _handleAddNewPress = () => {
    _toggleComingSoonModal();
  };

  const _handleSyncFromContactPress = () => {
    _toggleComingSoonModal();
  };

  return (
    <View style={styles.container}>
      <View horizontal alignItems="center" justifyContent="space-between">
        <Text marginVertical={12} fontSize={10} color={Colors.label}>
          <FormattedMessage defaultMessage="My Contacts" />
        </Text>
        <Touchable paddingVertical={12} onPress={_handleSyncFromContactPress}>
          <Text fontSize={10} color={Colors.secondary}>
            <FormattedMessage defaultMessage="Sync from Contact" />
          </Text>
        </Touchable>
      </View>
      <LinkButton
        icon={<PlusIcon />}
        title={<FormattedMessage defaultMessage="Add New" />}
        onPress={_handleAddNewPress}
      />
      <ComingSoonModal
        visible={comingSoonModal}
        onCancel={_toggleComingSoonModal}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 12,
    marginBottom: 6,
  },
});

export default ProfileContactListHeader;
