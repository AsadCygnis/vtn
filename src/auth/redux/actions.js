import {Alert} from 'react-native';
import firebaseAuth from '@react-native-firebase/auth';

import * as constants from './constants';
// import request from '../../util/request';
import {updateUser} from '../../settings/redux/actions';
import {UsersCollection} from '../../config/firebase';

/**
 * AUTH_STATE_CHANGE
 */
export const changeAuthState = payload => ({
  type: constants.AUTH_STATE_CHANGE,
  payload,
});

/**
 * EMAIL_PASSWORD_LOGIN
 */
export const loginWithEmailAndPassword =
  (email, password) => async dispatch => {
    try {
      dispatch({type: constants.EMAIL_PASSWORD_LOGIN.REQUEST});

      await firebaseAuth().signInWithEmailAndPassword(email, password);

      dispatch({
        type: constants.EMAIL_PASSWORD_LOGIN.SUCCESS,
        payload: firebaseAuth().currentUser.toJSON(),
      });
    } catch (error) {
      dispatch({type: constants.EMAIL_PASSWORD_LOGIN.FAIL, error});
      const message = error.code
        ? error.code === 'auth/user-not-found'
          ? 'Invalid email or password'
          : error.message.slice(error.code.length + 3)
        : error.message;
      Alert.alert('Login failed', message);
    } finally {
      dispatch({type: constants.EMAIL_PASSWORD_LOGIN.COMPLETE});
    }
  };

/**
 * EMAIL_PASSWORD_REGISTER
 */
export const registerWithEmailAndPassword = info => async dispatch => {
  try {
    dispatch({type: constants.EMAIL_PASSWORD_REGISTER.REQUEST});

    const {user} = await firebaseAuth().createUserWithEmailAndPassword(
      info.email,
      info.password,
    );
    delete info.password;
    await user.updateProfile({
      displayName: `${info.firstName} ${info.lastName}`,
    });
    await UsersCollection.doc(user.uid).set(info);

    dispatch({
      type: constants.EMAIL_PASSWORD_REGISTER.SUCCESS,
      payload: firebaseAuth().currentUser.toJSON(),
    });
  } catch (error) {
    dispatch({type: constants.EMAIL_PASSWORD_REGISTER.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('Registration failed', message);
  } finally {
    dispatch({type: constants.EMAIL_PASSWORD_REGISTER.COMPLETE});
  }
};

/**
 * PASSWORD_RESET_EMAIL_SEND
 */
export const sendPasswordResetEmail = (email, cb) => async dispatch => {
  try {
    dispatch({type: constants.PASSWORD_RESET_EMAIL_SEND.REQUEST});

    await firebaseAuth().sendPasswordResetEmail(email);

    dispatch({
      type: constants.PASSWORD_RESET_EMAIL_SEND.SUCCESS,
    });
    if (cb) {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.PASSWORD_RESET_EMAIL_SEND.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('Error', message);
  } finally {
    dispatch({type: constants.PASSWORD_RESET_EMAIL_SEND.COMPLETE});
  }
};

/**
 * PHONE_NUMBER_VERIFY
 */
export const verifyPhoneNumber =
  (phoneNumber, forceResend, cb) => async dispatch => {
    try {
      dispatch({type: constants.PHONE_NUMBER_VERIFY.REQUEST});

      // const phoneValidation = await request({
      //   url: '/user/validate_phone_number',
      //   method: 'POST',
      //   data: {phoneNumber},
      // });
      // if (phoneValidation.message !== 'OK') {
      //   throw new Error(phoneValidation.message);
      // }

      await firebaseAuth()
        .verifyPhoneNumber(phoneNumber, 5, forceResend)
        .then(
          phoneAuthSnapshot => {
            dispatch({type: constants.PHONE_NUMBER_VERIFY.SUCCESS});
            if (cb) {
              cb(phoneAuthSnapshot);
            }
          },
          error => {
            const message = error.code
              ? error.code === 'auth/user-not-found'
                ? 'Invalid email or password'
                : error.message.slice(error.code.length + 3)
              : error.message;
            Alert.alert('Error', message);
          },
        )
        .catch(error => {
          const message = error.code
            ? error.code === 'auth/user-not-found'
              ? 'Invalid email or password'
              : error.message.slice(error.code.length + 3)
            : error.message;
          Alert.alert('Error', message);
        });
    } catch (error) {
      dispatch({type: constants.PHONE_NUMBER_VERIFY.FAIL, error});
      Alert.alert('Error', error.message);
    } finally {
      dispatch({type: constants.PHONE_NUMBER_VERIFY.COMPLETE});
    }
  };

/**
 * PHONE_NUMBER_REGISTER
 */
export const registerPhoneNumber =
  (verificationId, code, phone, cb) => async dispatch => {
    try {
      dispatch({type: constants.PHONE_NUMBER_REGISTER.REQUEST});

      const credential = firebaseAuth.PhoneAuthProvider.credential(
        verificationId,
        code,
      );
      const payload = await firebaseAuth().currentUser.linkWithCredential(
        credential,
      );

      dispatch(
        updateUser({
          phone: {
            code: phone.code,
            number: phone.number,
            e164: `${phone.code}${phone.number}`,
          },
        }),
      );
      dispatch({
        type: constants.PHONE_NUMBER_REGISTER.SUCCESS,
        payload: payload.user.toJSON(),
      });
      if (typeof cb === 'function') {
        cb();
      }
    } catch (error) {
      dispatch({type: constants.PHONE_NUMBER_REGISTER.FAIL, error});
      const message = error.code
        ? error.message.slice(error.code.length + 3)
        : error.message;
      Alert.alert('Error', message);
    } finally {
      dispatch({type: constants.PHONE_NUMBER_REGISTER.COMPLETE});
    }
  };

/**
 * SECURITY_GET
 */
export const getSecurity = () => async dispatch => {
  try {
    dispatch({type: constants.SECURITY_GET.REQUEST});

    const uid = firebaseAuth().currentUser.uid;
    const securitySnapshot = await UsersCollection.doc(uid)
      .collection('settings')
      .doc('security')
      .get();

    if (securitySnapshot.exists) {
      dispatch({
        type: constants.SECURITY_GET.SUCCESS,
        payload: securitySnapshot.data(),
      });
    }
  } catch (e) {
    dispatch({type: constants.SECURITY_GET.FAIL});
  } finally {
    dispatch({type: constants.SECURITY_GET.COMPLETE});
  }
};

/**
 * SECURITY_UPDATE
 */
export const updateSecurity = (update, cb) => async dispatch => {
  try {
    dispatch({type: constants.SECURITY_UPDATE.REQUEST});

    const uid = firebaseAuth().currentUser.uid;
    await UsersCollection.doc(uid)
      .collection('settings')
      .doc('security')
      .update(update);

    dispatch({
      type: constants.SECURITY_UPDATE.SUCCESS,
      payload: update,
    });
    if (typeof cb === 'function') {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.SECURITY_UPDATE.FAIL});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('Error', message);
  } finally {
    dispatch({type: constants.SECURITY_UPDATE.COMPLETE});
  }
};

/**
 * APP_INTRO_SET
 */
export const setAppIntro = payload => ({
  type: constants.APP_INTRO_SET,
  payload,
});

/**
 * EMAIL_UPDATE
 */
export const updateEmail = (email, cb) => async dispatch => {
  try {
    dispatch({type: constants.EMAIL_UPDATE.REQUEST});

    await firebaseAuth().currentUser.updateEmail(email);

    dispatch(updateUser({email}));
    dispatch({
      type: constants.EMAIL_UPDATE.SUCCESS,
      payload: email,
    });
    if (typeof cb === 'function') {
      cb();
    }
  } catch (e) {
    dispatch({type: constants.EMAIL_UPDATE.FAIL});
    Alert.alert('Error', 'Unable to process the request');
  } finally {
    dispatch({type: constants.EMAIL_UPDATE.COMPLETE});
  }
};

/**
 * PHONE_NUMBER_UPDATE
 */
export const updatePhoneNumber =
  (verificationId, code, phone, cb) => async dispatch => {
    try {
      dispatch({type: constants.PHONE_NUMBER_UPDATE.REQUEST});

      const credential = firebaseAuth.PhoneAuthProvider.credential(
        verificationId,
        code,
      );
      await firebaseAuth().currentUser.updatePhoneNumber(credential);

      dispatch(
        updateUser({
          phone: {
            code: phone.code,
            number: phone.number,
            e164: `${phone.code}${phone.number}`,
          },
        }),
      );
      dispatch({
        type: constants.PHONE_NUMBER_UPDATE.SUCCESS,
        payload: `${phone.code}${phone.number}`,
      });
      if (typeof cb === 'function') {
        cb();
      }
    } catch (error) {
      dispatch({type: constants.PHONE_NUMBER_UPDATE.FAIL});
      const message = error.code
        ? error.message.slice(error.code.length + 3)
        : error.message;
      Alert.alert('Error', message);
    } finally {
      dispatch({type: constants.PHONE_NUMBER_UPDATE.COMPLETE});
    }
  };

/**
 * PROFILE_UPDATE
 */
export const updateProfile = update => async dispatch => {
  try {
    dispatch({type: constants.PROFILE_UPDATE.REQUEST});

    await firebaseAuth().currentUser.updateProfile(update);

    dispatch({
      type: constants.PROFILE_UPDATE.SUCCESS,
      payload: update,
    });
  } catch (e) {
    dispatch({type: constants.PROFILE_UPDATE.FAIL});
    Alert.alert('Error', 'Unable to process the request');
  } finally {
    dispatch({type: constants.PROFILE_UPDATE.COMPLETE});
  }
};

/**
 * PASSWORD_RESET
 */
export const resetPassword = (newPassword, cb) => async dispatch => {
  try {
    dispatch({type: constants.PASSWORD_RESET.REQUEST});

    const user = firebaseAuth().currentUser;
    await user.updatePassword(newPassword);

    dispatch({type: constants.PASSWORD_RESET.SUCCESS});
    if (cb) {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.PASSWORD_RESET.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('Error', message);
  } finally {
    dispatch({type: constants.PASSWORD_RESET.COMPLETE});
  }
};

/**
 * REFRESH
 */
export const refresh = () => async dispatch => {
  try {
    dispatch({type: constants.REFRESH.REQUEST});

    await firebaseAuth().currentUser.reload();

    const user = firebaseAuth().currentUser;

    if (user) {
      dispatch({
        type: constants.REFRESH.SUCCESS,
        payload: user.toJSON(),
      });
    }
  } catch (e) {
    dispatch({type: constants.REFRESH.FAIL});
  } finally {
    dispatch({type: constants.REFRESH.COMPLETE});
  }
};

/**
 * RE_AUTHENTICATE
 */
export const reAuthenticate = (password, cb) => async dispatch => {
  try {
    dispatch({type: constants.RE_AUTHENTICATE.REQUEST});

    const user = firebaseAuth().currentUser;
    const credential = firebaseAuth.EmailAuthProvider.credential(
      user.email,
      password,
    );
    const updatedCredential = await user.reauthenticateWithCredential(
      credential,
    );

    dispatch({
      type: constants.RE_AUTHENTICATE.SUCCESS,
      payload: updatedCredential.user.toJSON(),
    });
    if (cb) {
      cb();
    }
  } catch (error) {
    dispatch({type: constants.RE_AUTHENTICATE.FAIL, error});
    const message = error.code
      ? error.message.slice(error.code.length + 3)
      : error.message;
    Alert.alert('Error', message);
  } finally {
    dispatch({type: constants.RE_AUTHENTICATE.COMPLETE});
  }
};

/**
 * LOGOUT
 */
export const logout = () => async dispatch => {
  try {
    dispatch({type: constants.LOGOUT.REQUEST});

    const user = firebaseAuth().currentUser;
    if (user) {
      await firebaseAuth().signOut();
    }

    dispatch({type: constants.LOGOUT.SUCCESS});
  } catch (e) {
    dispatch({type: constants.LOGOUT.FAIL});
  } finally {
    dispatch({type: constants.LOGOUT.COMPLETE});
  }
};
