import {actionGenerator} from '../../util/reduxHelpers';

export const AUTH_STATE_CHANGE = 'AUTH/AUTH_STATE_CHANGE';
export const EMAIL_PASSWORD_LOGIN = actionGenerator(
  'AUTH/EMAIL_PASSWORD_LOGIN',
);
export const EMAIL_PASSWORD_REGISTER = actionGenerator(
  'AUTH/EMAIL_PASSWORD_REGISTER',
);
export const PASSWORD_RESET_EMAIL_SEND = actionGenerator(
  'AUTH/PASSWORD_RESET_EMAIL_SEND',
);
export const PHONE_NUMBER_VERIFY = actionGenerator('AUTH/PHONE_NUMBER_VERIFY');
export const PHONE_NUMBER_REGISTER = actionGenerator(
  'AUTH/PHONE_NUMBER_REGISTER',
);
export const SECURITY_GET = actionGenerator('AUTH/SECURITY_GET');
export const SECURITY_UPDATE = actionGenerator('AUTH/SECURITY_UPDATE');
export const APP_INTRO_SET = 'AUTH/APP_INTRO_SET';
export const EMAIL_UPDATE = actionGenerator('AUTH/EMAIL_UPDATE');
export const PHONE_NUMBER_UPDATE = actionGenerator('AUTH/PHONE_NUMBER_UPDATE');
export const PROFILE_UPDATE = actionGenerator('AUTH/PROFILE_UPDATE');
export const PASSWORD_RESET = actionGenerator('AUTH/PASSWORD_RESET');
export const REFRESH = actionGenerator('AUTH/REFRESH');
export const RE_AUTHENTICATE = actionGenerator('AUTH/RE_AUTHENTICATE');
export const LOGOUT = actionGenerator('AUTH/LOGOUT');
