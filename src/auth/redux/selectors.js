/**
 * Get user
 */
export const getUser = state => state.Auth.user;

/**
 * Get error
 */
export const getError = state => state.Auth.error;

/**
 * Get loading
 */
export const getLoading = state => state.Auth.loading;

/**
 * Get security
 */
export const getSecurity = state => state.Auth.security;

/**
 * Get setup quick access
 */
export const getAppIntro = state => state.Auth.appIntro;

/**
 * Get fully Authenticated
 */
export const getFullyAuthenticated = state =>
  !!getUser(state) && !!getUser(state).phoneNumber && !getAppIntro(state);
