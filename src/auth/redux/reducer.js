import * as constants from './constants';

export const INITIAL_STATE = {
  user: null,
  error: null,
  loading: false,
  security: {},
  verification: {},
  appIntro: false,
};

export default function reducer(state = INITIAL_STATE, action) {
  const {type, payload, error} = action;

  switch (type) {
    // AUTH_STATE_CHANGE
    case constants.AUTH_STATE_CHANGE:
      return {
        ...state,
        user: payload,
      };

    // EMAIL_PASSWORD_LOGIN
    case constants.EMAIL_PASSWORD_LOGIN.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.EMAIL_PASSWORD_LOGIN.FAIL:
      return {
        ...state,
        error,
      };
    case constants.EMAIL_PASSWORD_LOGIN.SUCCESS:
      return {
        ...state,
        user: payload,
      };
    case constants.EMAIL_PASSWORD_LOGIN.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // EMAIL_PASSWORD_REGISTER
    case constants.EMAIL_PASSWORD_REGISTER.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.EMAIL_PASSWORD_REGISTER.FAIL:
      return {
        ...state,
        error,
      };
    case constants.EMAIL_PASSWORD_REGISTER.SUCCESS:
      return {
        ...state,
        user: payload,
      };
    case constants.EMAIL_PASSWORD_REGISTER.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // PASSWORD_RESET_EMAIL_SEND
    case constants.PASSWORD_RESET_EMAIL_SEND.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.PASSWORD_RESET_EMAIL_SEND.FAIL:
      return {
        ...state,
        error,
      };
    case constants.PASSWORD_RESET_EMAIL_SEND.SUCCESS:
      return state;
    case constants.PASSWORD_RESET_EMAIL_SEND.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // PHONE_NUMBER_VERIFY
    case constants.PHONE_NUMBER_VERIFY.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.PHONE_NUMBER_VERIFY.FAIL:
      return {
        ...state,
        error,
      };
    case constants.PHONE_NUMBER_VERIFY.SUCCESS:
      return state;
    case constants.PHONE_NUMBER_VERIFY.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // PHONE_NUMBER_REGISTER
    case constants.PHONE_NUMBER_REGISTER.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.PHONE_NUMBER_REGISTER.FAIL:
      return {
        ...state,
        error,
      };
    case constants.PHONE_NUMBER_REGISTER.SUCCESS:
      return {
        ...state,
        user: payload,
        appIntro: true,
      };
    case constants.PHONE_NUMBER_REGISTER.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // SECURITY_GET
    case constants.SECURITY_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.SECURITY_GET.FAIL:
      return {
        ...state,
        error,
      };
    case constants.SECURITY_GET.SUCCESS:
      return {
        ...state,
        security: payload,
      };
    case constants.SECURITY_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // SECURITY_UPDATE
    case constants.SECURITY_UPDATE.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.SECURITY_UPDATE.FAIL:
      return {
        ...state,
        error,
      };
    case constants.SECURITY_UPDATE.SUCCESS:
      return {
        ...state,
        security: {
          ...state.security,
          ...payload,
        },
      };
    case constants.SECURITY_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // APP_INTRO_SET
    case constants.APP_INTRO_SET:
      return {
        ...state,
        appIntro: payload,
      };

    // EMAIL_UPDATE
    case constants.EMAIL_UPDATE.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.EMAIL_UPDATE.FAIL:
      return {
        ...state,
        error,
      };
    case constants.EMAIL_UPDATE.SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          email: payload,
        },
      };
    case constants.EMAIL_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // PHONE_NUMBER_UPDATE
    case constants.PHONE_NUMBER_UPDATE.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.PHONE_NUMBER_UPDATE.FAIL:
      return {
        ...state,
        error,
      };
    case constants.PHONE_NUMBER_UPDATE.SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          phoneNumber: payload,
        },
      };
    case constants.PHONE_NUMBER_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // PROFILE_UPDATE
    case constants.PROFILE_UPDATE.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.PROFILE_UPDATE.FAIL:
      return {
        ...state,
        error,
      };
    case constants.PROFILE_UPDATE.SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          ...payload,
        },
      };
    case constants.PROFILE_UPDATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // PASSWORD_RESET
    case constants.PASSWORD_RESET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.PASSWORD_RESET.FAIL:
      return {
        ...state,
        error,
      };
    case constants.PASSWORD_RESET.SUCCESS:
      return state;
    case constants.PASSWORD_RESET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // REFRESH
    case constants.REFRESH.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.REFRESH.FAIL:
      return {
        ...state,
        error,
      };
    case constants.REFRESH.SUCCESS:
      return {
        ...state,
        user: payload,
      };
    case constants.REFRESH.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // RE_AUTHENTICATE
    case constants.RE_AUTHENTICATE.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.RE_AUTHENTICATE.FAIL:
      return {
        ...state,
        error,
      };
    case constants.RE_AUTHENTICATE.SUCCESS:
      return {
        ...state,
        user: payload,
      };
    case constants.RE_AUTHENTICATE.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    // LOGOUT
    case constants.LOGOUT.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case constants.LOGOUT.FAIL:
      return {
        ...state,
        error,
      };
    case constants.LOGOUT.SUCCESS:
      return INITIAL_STATE;
    case constants.LOGOUT.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
