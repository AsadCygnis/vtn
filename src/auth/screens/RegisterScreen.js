import React, {useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import AuthHeader from '../components/AuthHeader';
import {
  View,
  Text,
  Select,
  Button,
  Content,
  TextInput,
  Touchable,
  DateTimePicker,
} from '../../common';
import * as Colors from '../../config/colors';

import {getLoading} from '../redux/selectors';
import {registerWithEmailAndPassword as registerWithEmailAndPasswordAction} from '../redux/actions';

/* =============================================================================
<RegisterScreen />
============================================================================= */
const RegisterScreen = ({loading, registerWithEmailAndPassword}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [gender, setGender] = useState('male');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [dob, setDob] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [referralCode, setReferralCode] = useState('');

  const _handleLoginPress = () => {
    navigation.navigate('Login');
  };

  const _handleRegisterPress = () => {
    if (!disabled) {
      registerWithEmailAndPassword({
        gender,
        firstName,
        lastName,
        dob,
        email,
        password,
        referralCode,
      });
    }
  };

  const styles = getStyles(insets);

  const disabled =
    !firstName ||
    !lastName ||
    !dob ||
    !gender ||
    !email ||
    !password ||
    loading;

  const genderData = [
    {
      label: intl.formatMessage({defaultMessage: 'Mr'}),
      value: 'male',
    },
    {
      label: intl.formatMessage({defaultMessage: 'Mrs'}),
      value: 'female',
    },
  ];

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.select({ios: 'padding', android: undefined})}>
      <Content paddingHorizontal={20}>
        <AuthHeader />
        <View style={styles.form}>
          <View style={styles.row}>
            <Select
              data={genderData}
              label={intl.formatMessage({defaultMessage: 'Title*'})}
              value={gender}
              disabled={false}
              placeholder={intl.formatMessage({defaultMessage: 'Select Title'})}
              containerStyle={styles.column}
              onChange={value => setGender(value)}
            />
            <TextInput
              label={intl.formatMessage({defaultMessage: 'First Name*'})}
              value={firstName}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter First Name',
              })}
              containerStyle={styles.column}
              onChange={setFirstName}
            />
          </View>
          <TextInput
            label={intl.formatMessage({defaultMessage: 'Last Name*'})}
            value={lastName}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter Last Name',
            })}
            onChange={setLastName}
          />
          <DateTimePicker
            label={intl.formatMessage({defaultMessage: 'Date of Birth*'})}
            value={dob}
            placeholder={intl.formatMessage({defaultMessage: 'Select Date'})}
            onChange={value => setDob(value)}
          />
          <TextInput
            label={intl.formatMessage({defaultMessage: 'Email Address*'})}
            value={email}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter Your Email',
            })}
            keyboardType="email-address"
            onChange={setEmail}
          />
          <TextInput
            label={intl.formatMessage({defaultMessage: 'Create Password*'})}
            value={password}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter a Strong Memorable Password',
            })}
            secureTextEntry
            onChange={setPassword}
          />
          <TextInput
            label={intl.formatMessage({
              defaultMessage: 'Referral Code (Optional)',
            })}
            value={referralCode}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter a Strong Memorable Password',
            })}
            onChange={setReferralCode}
          />
          <Touchable style={styles.termsLink}>
            <Text style={styles.link}>
              {intl.formatMessage({
                defaultMessage:
                  'By clicking "Create Account", you agree to our Terms and Conditions',
              })}
            </Text>
          </Touchable>
        </View>
        <Button
          block
          title={intl.formatMessage({defaultMessage: 'Register'})}
          style={styles.registerBtn}
          loading={loading}
          disabled={disabled}
          onPress={_handleRegisterPress}
        />
        <Touchable style={styles.loginLink} onPress={_handleLoginPress}>
          <Text>
            <Text style={styles.link} align="center">
              {intl.formatMessage({defaultMessage: 'Already have an account?'})}
            </Text>{' '}
            <Text style={styles.link} align="center" color={Colors.primary}>
              {intl.formatMessage({defaultMessage: 'Login'})}
            </Text>
          </Text>
        </Touchable>
      </Content>
    </KeyboardAvoidingView>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      marginTop: 12,
    },
    row: {
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: -6,
    },
    column: {
      width: null,
      flex: 1,
      paddingHorizontal: 6,
    },
    termsLink: {
      alignSelf: 'flex-start',
      marginTop: 16,
      marginBottom: 8,
      paddingVertical: 12,
    },
    link: {
      lineHeight: 16,
    },
    registerBtn: {
      marginBottom: 8,
    },
    loginLink: {
      alignSelf: 'center',
      paddingVertical: 12,
      marginBottom: insets.bottom || 10,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
});

const mapDispatchToProps = {
  registerWithEmailAndPassword: registerWithEmailAndPasswordAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
