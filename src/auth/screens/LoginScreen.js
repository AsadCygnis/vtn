import React, {useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import AuthHeader from '../components/AuthHeader';
import {View, Text, Button, Content, TextInput, Touchable} from '../../common';
import * as Colors from '../../config/colors';

import {getLoading as selectAuthLoading} from '../redux/selectors';
// import {getLoading as selectMyAccountLoading} from '../../profile/redux/selectors';
import {loginWithEmailAndPassword as loginWithEmailAndPasswordAction} from '../redux/actions';

/* =============================================================================
<LoginScreen />
============================================================================= */
const LoginScreen = ({loading, loginWithEmailAndPassword}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const disabled = !email || !password || loading;
  const styles = getStyles(insets);

  const _handleForgotPasswordPress = () => {
    navigation.navigate('RecoverAccount');
  };

  const _handleRegisterPress = () => {
    navigation.navigate('Register');
  };

  const _handleLoginPress = () => {
    if (!disabled) {
      loginWithEmailAndPassword(email, password);
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.select({ios: 'padding', android: undefined})}>
      <Content paddingHorizontal={20}>
        <AuthHeader />
        <View style={styles.form}>
          <TextInput
            label={intl.formatMessage({defaultMessage: 'Email Address'})}
            value={email}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter Your Email',
            })}
            keyboardType="email-address"
            onChange={setEmail}
          />
          <TextInput
            label={intl.formatMessage({defaultMessage: 'Account Password'})}
            value={password}
            placeholder={intl.formatMessage({
              defaultMessage: 'Enter Your Account Password',
            })}
            secureTextEntry
            onChange={setPassword}
          />
          <Touchable
            style={styles.forgotPasswordLink}
            onPress={_handleForgotPasswordPress}>
            <Text color={Colors.danger} fontFamily="Poppins-Bold">
              {intl.formatMessage({defaultMessage: 'Forgot Password?'})}
            </Text>
          </Touchable>
        </View>
        <Button
          block
          title={intl.formatMessage({defaultMessage: 'Login'})}
          style={styles.loginBtn}
          loading={loading}
          disabled={disabled}
          onPress={_handleLoginPress}
        />
        <Touchable style={styles.registerLink} onPress={_handleRegisterPress}>
          <Text>
            <Text style={styles.link} align="center">
              {intl.formatMessage({defaultMessage: "Don't have an account?"})}
            </Text>{' '}
            <Text style={styles.link} align="center" color={Colors.primary}>
              {intl.formatMessage({defaultMessage: 'Register'})}
            </Text>
          </Text>
        </Touchable>
      </Content>
    </KeyboardAvoidingView>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      marginTop: 12,
    },
    forgotPasswordLink: {
      alignSelf: 'flex-end',
      marginRight: -12,
      paddingVertical: 8,
      paddingHorizontal: 12,
    },
    loginBtn: {
      marginBottom: 8,
    },
    registerLink: {
      alignSelf: 'center',
      paddingVertical: 12,
      marginBottom: insets.bottom || 10,
    },
  });

const mapStateToProps = state => ({
  // loading: selectAuthLoading(state) || selectMyAccountLoading(state),
  loading: selectAuthLoading(state),
});

const mapDispatchToProps = {
  loginWithEmailAndPassword: loginWithEmailAndPasswordAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
