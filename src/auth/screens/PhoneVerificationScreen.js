import React, {useEffect, useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Alert, KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import AuthHeader from '../components/AuthHeader';
import {View, Text, Touchable, Content, CodeInput} from '../../common';
import * as AppColors from '../../config/colors';

import {getLoading} from '../redux/selectors';
import {
  verifyPhoneNumber as verifyPhoneNumberAction,
  registerPhoneNumber as registerPhoneNumberAction,
} from '../redux/actions';

/* =============================================================================
<PhoneVerificationScreen />
============================================================================= */
const PhoneVerificationScreen = ({
  route,
  loading,
  verifyPhoneNumber,
  registerPhoneNumber,
}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const [_code, setCode] = useState();
  const [resendTime, setResendTime] = useState(25);
  const [_verificationId, setVerificationId] = useState();
  const {code, phone, verificationId} = route.params;
  const phoneNumber = phone ? `${phone.code}${phone.number}` : '';
  const styles = getStyles(insets);

  // Sync state with props
  useEffect(() => {
    setCode(code);
    setVerificationId(verificationId);
  }, [verificationId, code]);

  // Register verified phone number
  useEffect(() => {
    if (_verificationId && _code) {
      registerPhoneNumber(_verificationId, _code, phone);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [_verificationId, _code]);

  // Start resend timer
  useEffect(() => {
    if (!verificationId && !code) {
      const interval = setInterval(() => {
        setResendTime(prevState => (prevState > 0 ? prevState - 1 : prevState));
      }, 1000);

      return () => {
        clearTimeout(interval);
      };
    }
  }, [verificationId, code]);

  const _handleFulfill = otp => {
    registerPhoneNumber(_verificationId, otp, phone);
  };

  const _handleResend = async () => {
    if (phoneNumber) {
      verifyPhoneNumber(phoneNumber, true, confirmation => {
        setCode(confirmation.code);
        setVerificationId(confirmation.verificationId);
        setResendTime(25);
      });
    } else {
      Alert.alert('Error', 'Please enter a valid phone number');
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.select({ios: 'padding', android: undefined})}>
      <Content paddingHorizontal={20}>
        <AuthHeader
          title={intl.formatMessage({defaultMessage: 'Verify Mobile Number'})}
          description={intl.formatMessage({
            defaultMessage:
              'We have sent you 6 digits verification code to your mobile, enter it below.',
          })}
        />
        <View style={styles.form}>
          <CodeInput
            codeLength={6}
            style={styles.codeInput}
            onFulfill={_handleFulfill}
          />
        </View>
        <Touchable
          style={styles.resendTime}
          disabled={loading || resendTime > 0}
          onPress={_handleResend}>
          {resendTime ? (
            <>
              <Text>{intl.formatMessage({defaultMessage: 'Resend In: '})}</Text>
              <Text marginHorizontal={2} color={AppColors.danger}>
                00:{resendTime < 10 ? `0${resendTime}` : resendTime}
              </Text>
            </>
          ) : (
            <Text>{intl.formatMessage({defaultMessage: 'Resend'})}</Text>
          )}
        </Touchable>
      </Content>
    </KeyboardAvoidingView>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      marginTop: 12,
    },
    codeInput: {
      marginTop: 10,
    },
    resendTime: {
      flexDirection: 'row',
      alignSelf: 'center',
      marginBottom: insets.bottom || 20,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
});

const mapDispatchToProps = {
  verifyPhoneNumber: verifyPhoneNumberAction,
  registerPhoneNumber: registerPhoneNumberAction,
};

/* Export
============================================================================= */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PhoneVerificationScreen);
