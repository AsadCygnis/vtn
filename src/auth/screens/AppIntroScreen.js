import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {View, Button, Content} from '../../common';
import AuthHeader from '../components/AuthHeader';
import CheckIcon from '../../assets/icons/edit-check-round.svg';

import {setAppIntro as setAppIntroAction} from '../redux/actions';

/* =============================================================================
<AppIntroScreen />
============================================================================= */
const AppIntroScreen = ({setAppIntro}) => {
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);

  const _handleGoToDashboardPress = () => {
    setAppIntro(false);
  };

  return (
    <Content paddingHorizontal={20}>
      <View flex={1} paddingVertical={40}>
        <AuthHeader
          title={<FormattedMessage defaultMessage="Registration Complete" />}
          logo={<CheckIcon />}
          description={
            <FormattedMessage defaultMessage="Thank you for registering Basic Account, you have unlocked very limited features at this time. Unlock more features by entering more details about you." />
          }
        />
      </View>
      <Button
        block
        title={<FormattedMessage defaultMessage="Dashboard" />}
        style={styles.submitBtn}
        onPress={_handleGoToDashboardPress}
      />
    </Content>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      paddingVertical: '5%',
    },
    submitBtn: {
      marginBottom: insets.bottom || 20,
    },
  });

const mapDispatchToProps = {
  setAppIntro: setAppIntroAction,
};

/* Export
============================================================================= */
export default connect(null, mapDispatchToProps)(AppIntroScreen);
