import React from 'react';
import {connect} from 'react-redux';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import LoginScreen from './LoginScreen';
import RegisterScreen from './RegisterScreen';
import RecoverAccountScreen from './RecoverAccountScreen';
import EnterPhoneNumberScreen from './EnterPhoneNumberScreen';
import PhoneVerificationScreen from './PhoneVerificationScreen';
import AppIntroScreen from './AppIntroScreen';

import {getUser, getAppIntro} from '../redux/selectors';

const Stack = createStackNavigator();

/* =============================================================================
<AuthStack />
============================================================================= */
const AuthStack = ({authenticated, phoneVerified, appIntro}) => {
  return (
    <Stack.Navigator
      headerMode="none"
      screenOptions={TransitionPresets.SlideFromRightIOS}>
      {!authenticated ? (
        <>
          <Stack.Screen name="Login" component={LoginScreen} />
          <Stack.Screen name="Register" component={RegisterScreen} />
          <Stack.Screen
            name="RecoverAccount"
            component={RecoverAccountScreen}
          />
        </>
      ) : !phoneVerified ? (
        <>
          <Stack.Screen
            name="EnterPhoneNumber"
            component={EnterPhoneNumberScreen}
          />
          <Stack.Screen
            name="PhoneVerification"
            component={PhoneVerificationScreen}
          />
        </>
      ) : appIntro ? (
        <Stack.Screen name="AppIntro" component={AppIntroScreen} />
      ) : (
        <Stack.Screen name="Empty" component={EmptyScreen} />
      )}
    </Stack.Navigator>
  );
};

const EmptyScreen = () => null;

const mapStateToProps = state => {
  const user = getUser(state);
  return {
    authenticated: !!user,
    phoneVerified: !!user && !!user.phoneNumber,
    appIntro: getAppIntro(state),
  };
};

const propsAreEqual = (prevProps, nextProps) =>
  prevProps.authenticated === nextProps.authenticated &&
  prevProps.phoneVerified === nextProps.phoneVerified &&
  prevProps.appIntro === nextProps.appIntro;

export default connect(mapStateToProps)(React.memo(AuthStack, propsAreEqual));
