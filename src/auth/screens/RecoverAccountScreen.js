import React, {useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {KeyboardAvoidingView, Platform, StyleSheet} from 'react-native';

import {View, Button, TextInput, Content} from '../../common';
import AuthHeader from '../components/AuthHeader';

import {getLoading} from '../redux/selectors';
import {sendPasswordResetEmail as sendPasswordResetEmailAction} from '../redux/actions';

/* =============================================================================
<RecoverAccountScreen />
============================================================================= */
const RecoverAccountScreen = ({loading, sendPasswordResetEmail}) => {
  const intl = useIntl();
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [codeSent, setCodeSent] = useState(false);
  const disabled = !email || loading;
  const styles = getStyles(insets);

  const _handleLoginPress = () => {
    navigation.navigate('Login');
  };

  const _handleSendPress = () => {
    if (!disabled) {
      sendPasswordResetEmail(email, () => {
        setCodeSent(true);
      });
    }
  };

  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.select({ios: 'padding', android: undefined})}>
      <Content paddingHorizontal={20}>
        <AuthHeader
          title={intl.formatMessage({defaultMessage: 'Recover Account'})}
          description={
            codeSent
              ? intl.formatMessage({
                  defaultMessage:
                    'We have sent you an email to your registered email address, Please follow the instructions sent to your email to recover your account.',
                })
              : intl.formatMessage({
                  defaultMessage:
                    'We will send you instructions to recover your account, Please enter your email address below and press Send.',
                })
          }
        />
        {!codeSent && (
          <View style={styles.form}>
            <TextInput
              value={email}
              placeholder={intl.formatMessage({
                defaultMessage: 'Enter your email',
              })}
              keyboardType="email-address"
              containerStyle={styles.inputContainer}
              onChange={setEmail}
              onSubmitEditing={() => setCodeSent(true)}
            />
          </View>
        )}
        {codeSent ? (
          <Button
            block
            title={intl.formatMessage({defaultMessage: 'Back to Login'})}
            style={styles.submitBtn}
            loading={loading}
            disabled={disabled}
            onPress={_handleLoginPress}
          />
        ) : (
          <Button
            block
            title={intl.formatMessage({defaultMessage: 'Send'})}
            style={styles.submitBtn}
            loading={loading}
            disabled={disabled}
            onPress={_handleSendPress}
          />
        )}
      </Content>
    </KeyboardAvoidingView>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      flex: 1,
    },
    form: {
      flex: 1,
      marginTop: 12,
    },
    inputContainer: {
      marginTop: '25%',
    },
    submitBtn: {
      alignSelf: 'center',
      paddingVertical: 12,
      marginBottom: insets.bottom || 10,
    },
  });

const mapStateToProps = state => ({
  loading: getLoading(state),
});

const mapDispatchToProps = {
  sendPasswordResetEmail: sendPasswordResetEmailAction,
};

/* Export
============================================================================= */
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RecoverAccountScreen);
