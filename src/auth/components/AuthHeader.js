import React from 'react';
import {StyleSheet} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {Text, View} from '../../common';
import AppLogo from '../../assets/icons/app-logo.svg';

/* =============================================================================
<AuthHeader />
============================================================================= */
const AuthHeader = ({logo, title, description}) => {
  const insets = useSafeAreaInsets();
  const layout = {
    paddingTop: insets.top + 30,
  };

  return (
    <View style={[styles.container, layout]}>
      {logo}
      {!!title && <Text style={styles.title}>{title}</Text>}
      {!!description && <Text style={styles.description}>{description}</Text>}
    </View>
  );
};

AuthHeader.defaultProps = {
  logo: <AppLogo />,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingBottom: 12,
  },
  title: {
    marginTop: 14,
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
  },
  description: {
    marginTop: 12,
    textAlign: 'center',
  },
});

/* Export
============================================================================= */
export default AuthHeader;
