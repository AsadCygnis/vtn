import {combineReducers} from 'redux';

import AuthReducer from '../auth/redux/reducer';
import RewardsReducer from '../rewards/redux/reducer';
import ContactsReducer from '../contacts/redux/reducer';
import ReferralReducer from '../referral/redux/reducer';
import SettingsReducer from '../settings/redux/reducer';
import LinkedAccountReducer from '../linkedAccount/redux/reducer';

/* ============================================================================
  Combine ALl Reducers
============================================================================= */
const rootReducer = combineReducers({
  Auth: AuthReducer,
  Rewards: RewardsReducer,
  Contacts: ContactsReducer,
  Referral: ReferralReducer,
  Settings: SettingsReducer,
  LinkedAccount: LinkedAccountReducer,
});

export default rootReducer;
