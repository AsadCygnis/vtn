import React, {useRef, useState} from 'react';
import {
  View,
  Text,
  Pressable,
  StyleSheet,
  TextInput as RNTextInput,
} from 'react-native';

import * as Colors from '../config/colors';

/* =============================================================================
<TextArea />
============================================================================= */
const TextArea = ({
  left,
  right,
  label,
  value,
  editable,
  inputStyle,
  labelStyle,
  placeholder,
  numberOfLines,
  containerStyle,
  contentContainerStyle,
  onChange,
  ...props
}) => {
  const textInput = useRef();
  const [focused, setFocused] = useState(false);

  const _handleChange = inputValue => {
    if (onChange) {
      onChange(inputValue);
    }
  };

  const _handlePress = () => {
    if (textInput.current) {
      textInput.current.focus();
    }
  };

  const _style = {
    borderColor: focused ? Colors.primary : Colors.border,
  };

  return (
    <Pressable
      style={[styles.container, containerStyle]}
      disabled={!editable}
      onPress={_handlePress}>
      {!!label && <Text style={[styles.label, labelStyle]}>{label}</Text>}
      <View style={[styles.content, _style, contentContainerStyle]}>
        {left}
        <RNTextInput
          ref={textInput}
          value={value}
          style={[
            styles.input,
            left && styles.inputWithLeft,
            right && styles.inputWithRight,
            inputStyle,
          ]}
          editable={editable}
          multiline={true}
          numberOfLines={numberOfLines}
          selectionColor="#8A93A0"
          placeholderTextColor={Colors.placeholder}
          placeholder={placeholder}
          onBlur={() => setFocused(false)}
          onFocus={() => setFocused(true)}
          onChangeText={_handleChange}
          {...props}
        />
        {right}
      </View>
    </Pressable>
  );
};

TextArea.defaultProps = {
  editable: true,
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 12,
  },
  label: {
    marginBottom: 6,
    color: Colors.label,
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    lineHeight: 18,
  },
  content: {
    width: '100%',
    minHeight: 150,
    flexDirection: 'row',
    paddingTop: 6,
    paddingBottom: 14,
    paddingHorizontal: 14,
    borderWidth: 0.5,
    borderRadius: 4,
    backgroundColor: Colors.card,
  },
  input: {
    flex: 1,
    color: Colors.text,
    fontSize: 12,
    textAlignVertical: 'top',
    fontFamily: 'Poppins-Regular',
  },
  inputWithLeft: {
    marginLeft: 14,
  },
  inputWithRight: {
    marginRight: 14,
  },
});

export default TextArea;
