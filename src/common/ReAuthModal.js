import React, {useState} from 'react';
import Modal from 'react-native-modal';
import {StyleSheet} from 'react-native';
import firebaseAuth from '@react-native-firebase/auth';
import {FormattedMessage, useIntl} from 'react-intl';

import View from './View';
import Text from './Text';
import Button from './Button';
import TextInput from './TextInput';
import * as Colors from '../config/colors';

/* =============================================================================
<ReAuthModal />
============================================================================= */
const ReAuthModal = ({isVisible, onCancel, onFinish}) => {
  const intl = useIntl();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [password, setPassword] = useState('');
  const disabled = !password || loading;

  const _handleSubmit = async () => {
    if (!disabled) {
      setError(null);
      setLoading(true);
      try {
        const user = firebaseAuth().currentUser;
        const credential = firebaseAuth.EmailAuthProvider.credential(
          user.email,
          password,
        );
        const res = await user.reauthenticateWithCredential(credential);
        setPassword('');
        onFinish(res.user);
      } catch (e) {
        const message = e.code ? e.message.slice(e.code.length + 3) : e.message;
        setError(message);
      }
      setLoading(false);
    }
  };

  return (
    <Modal
      style={styles.modal}
      isVisible={isVisible}
      backdropColor="#000"
      useNativeDriver
      backdropOpacity={0.8}
      onBackdropPress={onCancel}
      onBackButtonPress={onCancel}>
      <View />
      <View style={styles.content}>
        <View style={styles.header}>
          <Text style={styles.title}>
            <FormattedMessage defaultMessage="Authentication Required" />
          </Text>
        </View>
        <TextInput
          value={password}
          placeholder={intl.formatMessage({
            defaultMessage: 'Account Password',
          })}
          containerStyle={styles.inputContainer}
          secureTextEntry
          onChange={setPassword}
          onSubmitEditing={_handleSubmit}
        />
        {!!error && <Text style={styles.error}>{error}</Text>}
        <Button
          title={intl.formatMessage({defaultMessage: 'Submit'})}
          style={styles.submitBtn}
          loading={loading}
          disabled={disabled}
          onPress={_handleSubmit}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center',
    margin: 0,
    paddingHorizontal: 30,
  },
  content: {
    paddingBottom: 12,
    paddingHorizontal: 16,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: Colors.border,
    backgroundColor: Colors.card,
  },
  header: {
    paddingTop: 20,
    paddingBottom: 12,
    paddingHorizontal: 16,
    marginHorizontal: -16,
    borderBottomWidth: 2,
    borderBottomColor: Colors.placeholder,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  title: {
    color: Colors.text,
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
  },
  inputContainer: {
    marginTop: 24,
  },
  error: {
    marginTop: 12,
    color: '#F62448',
  },
  submitBtn: {
    height: 50,
    marginTop: 20,
    marginBottom: 8,
  },
});

export default ReAuthModal;
