import React from 'react';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import View from './View';
import Text from './Text';
import Touchable from './Touchable';
import * as Colors from '../config/colors';
import CloseIcon from '../assets/icons/nav-close.svg';

/* =============================================================================
<ModalHeader />
============================================================================= */
const ModalHeader = ({title, left, right, onCancel}) => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const styles = getStyles(insets);

  const _handleBack = () => {
    if (typeof onCancel === 'function') {
      onCancel();
    } else {
      navigation.goBack();
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.left}>
        {left !== null &&
          (left || (
            <Touchable style={styles.backBtn} onPress={_handleBack}>
              <CloseIcon />
            </Touchable>
          ))}
      </View>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.right}>{right}</View>
    </View>
  );
};

const getStyles = insets =>
  StyleSheet.create({
    container: {
      position: 'relative',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingTop: insets.top + 20,
      paddingBottom: 12,
      backgroundColor: Colors.background,
    },
    backBtn: {
      paddingVertical: 12,
      paddingHorizontal: 20,
      zIndex: 2,
    },
    title: {
      position: 'absolute',
      top: insets.top + 24,
      left: 0,
      right: 0,
      fontSize: 16,
      textAlign: 'center',
      fontFamily: 'Poppins-SemiBold',
      zIndex: 1,
    },
    left: {
      zIndex: 2,
    },
    right: {
      zIndex: 2,
    },
  });

export default ModalHeader;
