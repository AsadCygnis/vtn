import React from 'react';
import RNModal from 'react-native-modal';
import {StyleSheet} from 'react-native';

import View from './View';
import Text from './Text';
import Button from './Button';
import * as Colors from '../config/colors';
import {FormattedMessage} from 'react-intl';

/* =============================================================================
<ComingSoonModal />
============================================================================= */
const ComingSoonModal = ({visible, onCancel}) => {
  return (
    <RNModal
      style={styles.modal}
      isVisible={visible}
      backdropColor="#000"
      useNativeDriver
      backdropOpacity={0.8}
      onBackdropPress={onCancel}
      onBackButtonPress={onCancel}>
      <View style={styles.content}>
        <View flex={1} justifyContent="space-between">
          <Text style={styles.title}>
            <FormattedMessage defaultMessage="Coming Soon" />
          </Text>
          <Text style={styles.text}>
            <FormattedMessage defaultMessage="We are working 24/7 to bring this functionality ot you ASAP" />
          </Text>
          <Button
            title={<FormattedMessage defaultMessage="Close" />}
            style={styles.submitBtn}
            onPress={onCancel}
          />
        </View>
      </View>
    </RNModal>
  );
};

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
    paddingBottom: 36,
    paddingHorizontal: 30,
  },
  content: {
    height: 400,
    alignItems: 'center',
    padding: 25,
    borderWidth: 1,
    borderRadius: 6,
    borderColor: Colors.cardBorder,
    backgroundColor: Colors.card,
  },
  title: {
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Poppins-SemiBold',
  },
  text: {
    fontSize: 24,
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
  },
  submitBtn: {
    marginTop: 30,
  },
});

export default ComingSoonModal;
