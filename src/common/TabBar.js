import React from 'react';
import {StyleSheet} from 'react-native';

import View from './View';
import Text from './Text';
import Touchable from './Touchable';
import * as Colors from '../config/colors';

/* =============================================================================
<TabBar />
============================================================================= */
const TabBar = ({style, navigationState, jumpTo}) => {
  return (
    <View style={[styles.container, style]}>
      {navigationState.routes.map((route, index) => (
        <Touchable
          flex={1}
          key={route.key}
          disabled={index === navigationState.index}
          alignItems="center"
          onPress={() => jumpTo(route.key)}>
          <View
            style={[
              styles.item,
              index === navigationState.index && styles.itemActive,
            ]}>
            <Text
              defaultMessage={route.title}
              type="h1"
              style={[
                styles.text,
                index === navigationState.index && styles.textActive,
              ]}
            />
          </View>
        </Touchable>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 64,
    flexDirection: 'row',
    backgroundColor: Colors.card,
  },
  item: {
    height: 64,
    justifyContent: 'center',
    paddingTop: 2,
    borderBottomWidth: 2,
    borderBottomColor: 'transparent',
  },
  itemActive: {
    borderBottomColor: Colors.border,
  },
  text: {
    color: Colors.text,
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
  },
  textActive: {
    fontFamily: 'Poppins-Bold',
    color: Colors.primary,
  },
});

export default TabBar;
