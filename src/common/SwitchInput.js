import React from 'react';
import {StyleSheet, Switch} from 'react-native';

import View from './View';
import Text from './Text';
import * as Colors from '../config/colors';

/* =============================================================================
<SwitchInput />
============================================================================= */
const SwitchInput = ({
  title,
  value,
  titleStyle,
  description,
  containerStyle,
  onChange,
}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <View horizontal center>
        <Text style={[styles.title, titleStyle]}>{title}</Text>
        <Switch
          value={value}
          thumbColor={Colors.primary}
          trackColor={{
            true: Colors.card,
            false: Colors.card,
          }}
          onValueChange={onChange}
        />
      </View>
      {!!description && <Text style={styles.description}>{description}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
  },
  title: {
    flex: 1,
    fontSize: 12,
    fontFamily: 'Poppins-SemiBold',
  },
  description: {
    marginTop: 20,
    lineHeight: 18,
  },
});

export default SwitchInput;
