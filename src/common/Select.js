import React from 'react';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {Text, View, StyleSheet} from 'react-native';

import * as Colors from '../config/colors';

/* =============================================================================
<Select />
============================================================================= */
const Select = ({
  data,
  label,
  value,
  disabled,
  textStyle,
  labelStyle,
  placeholder,
  containerStyle,
  placeHolderStyle,
  contentContainerStyle,
  onChange,
}) => {
  const _handleChange = inputValue => {
    if (onChange) {
      onChange(inputValue);
    }
  };

  return (
    <RNPickerSelect
      items={data}
      value={value}
      placeholder={{
        label: placeholder || 'Select',
        value: null,
      }}
      disabled={disabled}
      onValueChange={_handleChange}>
      <View
        style={[
          styles.container,
          value ? styles.active : styles.inactive,
          containerStyle,
        ]}>
        {!!label && <Text style={[styles.label, labelStyle]}>{label}</Text>}
        <View style={[styles.content, contentContainerStyle]}>
          {!!value && (
            <Text numberOfLines={1} style={[styles.value, textStyle]}>
              {data.find(item => item.value === value).label}
            </Text>
          )}
          {!value && placeholder && (
            <Text
              numberOfLines={1}
              style={[styles.placeholder, placeHolderStyle]}>
              {placeholder}
            </Text>
          )}
          <FontAwesome5Icon name="chevron-down" color={Colors.text} />
        </View>
      </View>
    </RNPickerSelect>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 12,
  },
  label: {
    marginBottom: 6,
    color: Colors.label,
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    lineHeight: 18,
  },
  content: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 14,
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: Colors.border,
    backgroundColor: Colors.card,
  },
  value: {
    marginRight: 14,
    fontSize: 12,
    color: Colors.text,
    fontFamily: 'Poppins-Regular',
  },
  placeholder: {
    marginRight: 4,
    fontSize: 12,
    color: Colors.placeholder,
    fontFamily: 'Poppins-Medium',
  },
});

export default Select;
