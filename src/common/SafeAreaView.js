import React from 'react';
import {SafeAreaView as RNSafeAreaView, StyleSheet} from 'react-native';

import * as Colors from '../config/colors';

/* =============================================================================
<SafeAreaView />
============================================================================= */
const SafeAreaView = ({children, backgroundColor}) => {
  const _style = {
    backgroundColor: backgroundColor || Colors.background,
  };

  return (
    <RNSafeAreaView style={[styles.safeArea, _style]}>
      {children}
    </RNSafeAreaView>
  );
};

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
  },
});

export default SafeAreaView;
