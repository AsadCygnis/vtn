import React from 'react';
import RNCheckBox from 'react-native-check-box';
import {Text, StyleSheet, TouchableWithoutFeedback} from 'react-native';

import * as Colors from '../config/colors';

/* =============================================================================
<Checkbox />
============================================================================= */
const Checkbox = ({label, checked, style, labelStyle, onChange}) => {
  const _handleChange = () => {
    if (onChange) {
      onChange(!checked);
    }
  };

  return (
    <RNCheckBox
      style={[styles.container, style]}
      isChecked={checked}
      rightTextView={<Text style={[styles.txt, labelStyle]}>{label}</Text>}
      Component={TouchableWithoutFeedback}
      checkBoxColor={Colors.border}
      onClick={_handleChange}
    />
  );
};

Checkbox.defaultProps = {
  checked: false,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 12,
  },
  iconContainer: {
    width: 18,
    height: 18,
    alignSelf: 'flex-start',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 6,
    borderColor: Colors.border,
    backgroundColor: Colors.background,
  },
  txt: {
    marginLeft: 8,
    color: Colors.text,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
});

export default Checkbox;
