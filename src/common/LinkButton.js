import React from 'react';
import {StyleSheet} from 'react-native';

import Text from './Text';
import Button from './Button';
import Touchable from './Touchable';
import * as Colors from '../config/colors';

/* =============================================================================
<LinkButton />
============================================================================= */
const LinkButton = ({
  icon,
  title,
  titleStyle,
  containerStyle,
  iconContainerStyle,
  onPress,
}) => {
  return (
    <Touchable style={[styles.container, containerStyle]} onPress={onPress}>
      <Button
        style={[styles.iconContainer, iconContainerStyle]}
        onPress={onPress}>
        {icon}
      </Button>
      <Text
        style={titleStyle}
        color={Colors.primary}
        fontFamily="Poppins-Medium">
        {title}
      </Text>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {
    width: 34,
    height: 31,
    marginRight: 12,
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
});

export default LinkButton;
