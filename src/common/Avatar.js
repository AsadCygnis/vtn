import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

import * as Colors from '../config/colors';

/* =============================================================================
<Avatar />
============================================================================= */
const Avatar = ({uri, style, size, radius}) => {
  const _layout = {
    width: size,
    height: size,
    borderRadius: radius,
    backgroundColor: Colors.card,
  };

  if (uri) {
    return <Image style={[styles.image, _layout, style]} source={{uri}} />;
  }

  return (
    <View style={[styles.container, _layout, style]}>
      <FontAwesome5Icon
        name="user"
        size={Number(size) / 2}
        color={Colors.text}
      />
    </View>
  );
};

Avatar.defaultProps = {
  radius: 8,
  size: 42,
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.card,
  },
  image: {
    backgroundColor: Colors.card,
  },
});

export default Avatar;
