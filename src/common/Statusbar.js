import React from 'react';
import {StatusBar} from 'react-native';

import * as Colors from '../config/colors';

/* =============================================================================
<Statusbar />
============================================================================= */
const Statusbar = () => (
  <StatusBar
    barStyle={'dark-content'}
    translucent={true}
    backgroundColor="transparent"
  />
);

/* Export
============================================================================= */
export default Statusbar;
