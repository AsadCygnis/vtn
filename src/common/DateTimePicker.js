import React, {useState} from 'react';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {FormattedDate, FormattedTime} from 'react-intl';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';

import * as Colors from '../config/colors';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';

/* =============================================================================
<DateTimePicker />
============================================================================= */
const DateTimePicker = ({
  mode,
  label,
  value,
  format,
  labelStyle,
  placeholder,
  containerStyle,
  contentContainerStyle,
  onChange,
  ...props
}) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const _showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const _hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const _handleChange = inputValue => {
    _hideDatePicker();

    if (onChange) {
      onChange(inputValue);
    }
  };

  return (
    <TouchableOpacity
      onPress={_showDatePicker}
      style={[styles.container, containerStyle]}>
      {!!label && <Text style={[styles.label, labelStyle]}>{label}</Text>}
      <View style={[styles.content, contentContainerStyle]}>
        {value ? (
          <Text numberOfLines={1} style={styles.value}>
            {mode === 'date' && (
              <FormattedDate
                value={value}
                year="numeric"
                month="long"
                day="2-digit"
              />
            )}
            {mode === 'time' && <FormattedTime value={value} />}
            {mode === 'datetime' && (
              <>
                <FormattedDate
                  value={value}
                  year="numeric"
                  month="long"
                  day="2-digit"
                />{' '}
                <FormattedTime value={value} />
              </>
            )}
          </Text>
        ) : (
          <Text numberOfLines={1} style={styles.placeholder}>
            {placeholder}
          </Text>
        )}
        <FontAwesome5Icon name="chevron-down" color={Colors.text} />
      </View>
      <DateTimePickerModal
        mode={mode}
        isVisible={isDatePickerVisible}
        onConfirm={_handleChange}
        onCancel={_hideDatePicker}
        {...props}
      />
    </TouchableOpacity>
  );
};

DateTimePicker.defaultProps = {
  mode: 'date',
  format: 'DD/MM/YYYY',
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 12,
  },
  label: {
    marginBottom: 6,
    color: Colors.label,
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    lineHeight: 18,
  },
  content: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 14,
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: Colors.border,
    backgroundColor: Colors.card,
  },
  value: {
    color: Colors.text,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
  placeholder: {
    color: Colors.placeholder,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
});

export default DateTimePicker;
