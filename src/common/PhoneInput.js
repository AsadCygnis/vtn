import React, {useRef, useState} from 'react';
import RNPickerSelect from 'react-native-picker-select';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import {
  View,
  Text,
  Pressable,
  StyleSheet,
  TextInput as RNTextInput,
} from 'react-native';

import * as Colors from '../config/colors';
import countries from '../static/countries.json';

/* =============================================================================
<PhoneInput />
============================================================================= */
const PhoneInput = ({
  left,
  right,
  label,
  value,
  editable,
  dialCode,
  labelStyle,
  inputStyle,
  placeholder,
  dialCodeStyle,
  containerStyle,
  inputContainerStyle,
  contentContainerStyle,
  dialCodeContainerStyle,
  onChange,
  onDialCodeChange,
  ...props
}) => {
  const textInput = useRef();
  const [focused, setFocused] = useState(false);
  const _style = {
    borderColor: focused ? Colors.primary : Colors.border,
  };

  const _handlePress = () => {
    if (textInput.current) {
      textInput.current.focus();
    }
  };

  const _handleChange = inputValue => {
    if (onChange) {
      onChange(inputValue);
    }
  };

  const _handleDialCodeChange = inputValue => {
    if (onDialCodeChange) {
      onDialCodeChange(inputValue);
    }
  };

  return (
    <View style={[styles.container, containerStyle]}>
      {!!label && <Text style={[styles.label, labelStyle]}>{label}</Text>}
      <View style={[styles.content, contentContainerStyle]}>
        <RNPickerSelect
          items={COUNTRY_CODES}
          value={dialCode}
          disabled={!editable}
          onValueChange={_handleDialCodeChange}>
          <View style={[styles.dialCodeContainer, dialCodeContainerStyle]}>
            <Text
              numberOfLines={1}
              style={[styles.dialCodeValue, dialCodeStyle]}>
              {!!dialCode &&
                COUNTRY_CODES.filter(item => item.value === dialCode)[0].value}
            </Text>
            <FontAwesome5Icon name="chevron-down" color={Colors.text} />
          </View>
        </RNPickerSelect>
        <Pressable
          style={[styles.inputContainer, _style, inputContainerStyle]}
          onPress={_handlePress}>
          {left}
          <RNTextInput
            ref={textInput}
            value={value}
            style={[
              styles.input,
              left && styles.inputWithLeft,
              right && styles.inputWithRight,
              inputStyle,
            ]}
            editable={editable}
            keyboardType={'phone-pad'}
            selectionColor={Colors.primary}
            placeholder={placeholder}
            placeholderTextColor={Colors.placeholder}
            onBlur={() => setFocused(false)}
            onFocus={() => setFocused(true)}
            onChangeText={_handleChange}
            {...props}
          />
          {right}
        </Pressable>
      </View>
    </View>
  );
};

PhoneInput.defaultProps = {
  editable: true,
};

const COUNTRY_CODES = countries.map(item => ({
  label: item.name,
  key: item.code,
  value: item.dial_code,
}));

const styles = StyleSheet.create({
  container: {
    width: '100%',
    marginTop: 12,
  },
  label: {
    marginBottom: 6,
    color: Colors.label,
    fontSize: 10,
    fontFamily: 'Poppins-Regular',
    lineHeight: 18,
  },
  content: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputContainer: {
    height: 50,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 12,
    paddingHorizontal: 14,
    borderWidth: 0.5,
    borderRadius: 4,
    backgroundColor: Colors.card,
  },
  input: {
    height: 50,
    flex: 1,
    paddingVertical: 0,
    paddingHorizontal: 0,
    color: Colors.text,
    fontSize: 12,
    fontFamily: 'Poppins-Regular',
  },
  inputWithLeft: {
    marginLeft: 14,
  },
  inputWithRight: {
    marginRight: 14,
  },
  dialCodeContainer: {
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 14,
    borderWidth: 0.5,
    borderRadius: 4,
    borderColor: Colors.border,
    backgroundColor: Colors.card,
  },
  dialCodeValue: {
    marginRight: 6,
    fontSize: 12,
    color: Colors.text,
    fontFamily: 'Poppins-Regular',
  },
  arrowIcon: {
    marginLeft: 12,
  },
});

export default PhoneInput;
