import React from 'react';
import RNCodeInput from 'react-native-code-input';
import {StyleSheet} from 'react-native';

import * as Colors from '../config/colors';

/* =============================================================================
<CodeInput />
============================================================================= */
const CodeInput = ({style, ...props}) => {
  return (
    <RNCodeInput
      inputPosition="left"
      containerStyle={styles.inputContainer}
      codeInputStyle={[styles.input, style]}
      selectionColor={Colors.primary}
      {...props}
    />
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flex: 0,
    justifyContent: 'center',
    marginTop: 12,
  },
  input: {
    width: 50,
    height: 50,
    borderWidth: 1,
    borderRadius: 6,
    color: Colors.text,
    fontSize: 12,
    fontFamily: 'Poppins-Bold',
    borderColor: Colors.border,
    backgroundColor: Colors.card,
  },
});

export default CodeInput;
