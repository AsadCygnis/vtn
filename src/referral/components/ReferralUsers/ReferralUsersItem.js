import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {FormattedNumber} from 'react-intl';

import {Avatar, Text, View} from '../../../common';
import * as Colors from '../../../config/colors';

import {makeGetReferralById} from '../../redux/selectors';

/* =============================================================================
<ReferralUserItem />
============================================================================= */
const ReferralUserItem = ({id, referral, getUserReward}) => {
  const avatar = referral && referral._user ? referral._user.avatar : '';
  const reward = referral && referral.reward ? referral.reward : 0;
  const username =
    referral && referral._user
      ? `${referral._user.firstName} ${referral._user.lastName}`
      : '';

  return (
    <View style={styles.container}>
      <Avatar uri={avatar} size={44} radius={8} />
      <View
        flex={1}
        horizontal
        alignItems="center"
        justifyContent="space-between">
        <Text style={styles.username}>{username}</Text>
        <Text style={styles.reward}>
          + <FormattedNumber value={reward} style="currency" currency="usd" />
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 12,
  },
  avatar: {
    width: 44,
    height: 44,
    borderRadius: 10,
    backgroundColor: Colors.card,
  },
  username: {
    marginLeft: 12,
    textAlign: 'left',
    fontFamily: 'Poppins-SemiBold',
  },
  reward: {
    marginLeft: 12,
    textAlign: 'right',
    fontFamily: 'Poppins-SemiBold',
  },
});

const mapStateToProps = () => {
  const getReferralById = makeGetReferralById();
  return (state, {id}) => ({
    referral: getReferralById(state, {id}),
  });
};

export default connect(mapStateToProps)(ReferralUserItem);
