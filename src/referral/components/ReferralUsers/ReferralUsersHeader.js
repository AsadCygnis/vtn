import React from 'react';
import {StyleSheet} from 'react-native';
import {FormattedMessage} from 'react-intl';

import {Text, View} from '../../../common';
import * as Colors from '../../../config/colors';

/* =============================================================================
<ReferralUsersHeader />
============================================================================= */
const ReferralUsersHeader = () => (
  <View style={styles.container}>
    <Text style={styles.referredUsers}>
      <FormattedMessage defaultMessage="Referred Users" />
    </Text>
    <Text style={styles.rewardsEarned}>
      <FormattedMessage defaultMessage="Rewards Earned" />
    </Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 12,
    paddingBottom: 6,
  },
  referredUsers: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'left',
  },
  rewardsEarned: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'right',
  },
});

export default ReferralUsersHeader;
