import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';

import {View} from '../../../common';
import ReferralUsersItem from './ReferralUsersItem';
import ReferralUsersHeader from './ReferralUsersHeader';

import {getReferralIdsByMonth} from '../../redux/selectors';
import {getReferrals as getReferralsAction} from '../../redux/actions';

/* =============================================================================
<ReferralUsers />
============================================================================= */
const ReferralUsers = ({month, referrals, getReferrals}) => {
  // Get referrals for the month
  useEffect(() => {
    if (month) {
      getReferrals(month);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [month]);

  return (
    <View style={styles.container}>
      <ReferralUsersHeader />
      {referrals.map(id => (
        <ReferralUsersItem id={id} key={id} />
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 12,
    paddingHorizontal: 20,
  },
});

const mapStateToProps = (state, {month}) => ({
  referrals: getReferralIdsByMonth(state, {month}),
});

const mapDispatchToProps = {
  getReferrals: getReferralsAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(ReferralUsers);
