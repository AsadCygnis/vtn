import React from 'react';
import {useIntl} from 'react-intl';
import {StyleSheet} from 'react-native';

import {Select} from '../../common';
import * as Colors from '../../config/colors';

/* =============================================================================
<MyRewardsSortOrderSelect />
============================================================================= */
const MyRewardsSortOrderSelect = ({sortOrder, onSortOrderChange}) => {
  const intl = useIntl();
  const sortOrders = [
    {
      label: intl.formatMessage({defaultMessage: 'Newest to Oldest'}),
      value: 'descending',
    },
    {
      label: intl.formatMessage({defaultMessage: 'Oldest to Newest'}),
      value: 'ascending',
    },
  ];

  return (
    <Select
      data={sortOrders}
      value={sortOrder}
      editable={false}
      placeholder={intl.formatMessage({defaultMessage: 'Select One'})}
      containerStyle={styles.container}
      contentContainerStyle={styles.contentContainer}
      onChange={onSortOrderChange}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    width: null,
    marginHorizontal: 20,
  },
  contentContainer: {
    borderColor: Colors.card,
  },
});

/* Export
============================================================================= */
export default MyRewardsSortOrderSelect;
