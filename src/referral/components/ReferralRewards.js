import React from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';

import {Select, Text, View} from '../../common';
import * as Colors from '../../config/colors';

import {getRewardByMonth} from '../redux/selectors';

/* =============================================================================
<ReferralRewards />
============================================================================= */
const ReferralRewards = ({reward, month, months, onMonthChange}) => {
  const intl = useIntl();

  return (
    <View style={styles.container}>
      <View flex={1} style={styles.monthContainer}>
        <Select
          data={months.map(m => ({
            label: intl.formatDate(m.value, {
              month: 'long',
              year: 'numeric',
            }),
            value: m.label,
          }))}
          value={month}
          editable={false}
          placeholder={intl.formatMessage({defaultMessage: 'Select Month'})}
          containerStyle={styles.selectContainer}
          contentContainerStyle={styles.selectContentContainer}
          onChange={onMonthChange}
        />
      </View>
      <View>
        <Text color={Colors.label} fontSize={10} align="right">
          {intl.formatMessage({defaultMessage: 'Total Rewards'})}
        </Text>
        <Text
          color={Colors.primary}
          align="right"
          fontSize={19}
          fontFamily="Poppins-SemiBold">
          +{' '}
          {intl.formatNumber(reward, {
            style: 'currency',
            currency: 'usd',
            maximumFractionDigits: 2,
          })}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 18,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: Colors.placeholder,
  },
  monthContainer: {
    flex: 1,
    marginRight: 36,
  },
  selectContainer: {
    marginTop: 0,
  },
  selectContentContainer: {
    borderColor: Colors.card,
  },
});

const mapStateToProps = (state, {month}) => ({
  reward: getRewardByMonth(state, {month}),
});

/* Export
============================================================================= */
export default connect(mapStateToProps)(ReferralRewards);
