import React from 'react';
import {connect} from 'react-redux';
import {StyleSheet} from 'react-native';
import {FormattedDate, FormattedNumber, FormattedTime} from 'react-intl';

import {Text, Touchable, View} from '../../../common';
import * as Colors from '../../../config/colors';

import {makeGetRewardById} from '../../../rewards/redux/selectors';

/* =============================================================================
<MyRewardsListItem />
============================================================================= */
const MyRewardsListItem = ({reward}) => {
  const rewardType = reward ? reward.type : '';
  const date = reward ? reward.createdAt : new Date();
  const username =
    reward && reward.user
      ? `${reward.user.firstName} ${reward.user.lastName}`
      : '';
  const amount = reward ? `${reward.amount}` : 0;

  const _handlePress = () => {};

  return (
    <Touchable style={styles.container} onPress={_handlePress}>
      <View style={styles.row}>
        <Text style={styles.type}>{rewardType}</Text>
        <Text style={styles.date}>
          <FormattedDate
            value={date}
            day="2-digit"
            month="2-digit"
            year="numeric"
          />{' '}
          | <FormattedTime value={date} />
        </Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.username}>{username}</Text>
        <Text style={styles.amount}>
          +{' '}
          <FormattedNumber
            value={amount}
            style="currency"
            currency="usd"
            minimumFractionDigits={0}
            maximumFractionDigits={2}
          />
        </Text>
      </View>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 12,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  type: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'left',
  },
  date: {
    color: Colors.label,
    fontSize: 10,
    textAlign: 'right',
  },
  username: {
    textAlign: 'left',
    fontFamily: 'Poppins-SemiBold',
  },
  amount: {
    fontSize: 14,
    textAlign: 'right',
    fontFamily: 'Poppins-SemiBold',
  },
});

const mapStateToProps = () => {
  const getRewardById = makeGetRewardById();
  return (state, {id}) => ({
    reward: getRewardById(state, {id}),
  });
};

export default connect(mapStateToProps)(MyRewardsListItem);
