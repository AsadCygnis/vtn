import React from 'react';
import {StyleSheet} from 'react-native';
import {FormattedDate} from 'react-intl';

import {Text, View} from '../../../common';
import * as Colors from '../../../config/colors';

/* =============================================================================
<MyRewardsListSectionHeader />
============================================================================= */
const MyRewardsListSectionHeader = ({section}) => {
  return (
    <View style={styles.container}>
      <View style={styles.hr} />
      <Text style={styles.text}>
        <FormattedDate value={section.title} month="long" year="numeric" />
      </Text>
      <View style={styles.hr} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 12,
    marginBottom: 6,
  },
  hr: {
    flex: 1,
    height: 1,
    backgroundColor: Colors.placeholder,
  },
  text: {
    marginHorizontal: 6,
    color: Colors.label,
    fontSize: 10,
  },
});

export default MyRewardsListSectionHeader;
