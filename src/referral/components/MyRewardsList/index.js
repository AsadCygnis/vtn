import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {FormattedMessage} from 'react-intl';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {StyleSheet, SectionList} from 'react-native';

import {ListEmpty} from '../../../common';
import MyRewardsListItem from './MyRewardsListItem';
import MyRewardsListSectionHeader from './MyRewardsListSectionHeader';

import {getRewardIdsByMonth} from '../../../rewards/redux/selectors';
// import {getRewards as getRewardsAction} from '../../../rewards/redux/actions';

/* =============================================================================
<MyRewardsList />
============================================================================= */
const MyRewardsList = ({rewards, getRewards}) => {
  const insets = useSafeAreaInsets();
  const styles = getStyles(insets);

  // useEffect(() => {
  //   getRewards();
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  return (
    <SectionList
      sections={rewards}
      stickySectionHeadersEnabled={false}
      contentContainerStyle={styles.contentContainerStyle}
      renderItem={renderItem}
      keyExtractor={renderKeyExtractor}
      ListEmptyComponent={renderListEmpty}
      renderSectionHeader={renderSectionHeader}
    />
  );
};

const renderItem = ({item}) => <MyRewardsListItem id={item} />;
const renderListEmpty = () => (
  <ListEmpty title={<FormattedMessage defaultMessage="No Data" />} />
);
const renderKeyExtractor = item => `${item}`;
const renderSectionHeader = ({section}) => (
  <MyRewardsListSectionHeader section={section} />
);

const getStyles = insets =>
  StyleSheet.create({
    contentContainerStyle: {
      flexGrow: 1,
      paddingTop: 12,
      paddingBottom: insets.bottom + 12,
      paddingHorizontal: 20,
    },
  });

const mapStateToProps = (state, {sortOrder}) => ({
  rewards: getRewardIdsByMonth(state, {sortOrder}),
});

const mapDispatchToProps = {
  // getRewards: getRewardsAction,
};

/* Export
============================================================================= */
export default connect(mapStateToProps, mapDispatchToProps)(MyRewardsList);
