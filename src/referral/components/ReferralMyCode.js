import React, {useState} from 'react';
import {useIntl} from 'react-intl';
import {connect} from 'react-redux';
import Clipboard from '@react-native-clipboard/clipboard';
import {StyleSheet} from 'react-native';

import {Text, TextInput, Touchable} from '../../common';
import * as Colors from '../../config/colors';

import {getUser} from '../../auth/redux/selectors';

/* =============================================================================
<ReferralMyCode />
============================================================================= */
const ReferralMyCode = ({user}) => {
  const intl = useIntl();
  const [copied, setCopied] = useState(false);
  const referralCode = user ? user.uid : '';

  const _handleCopyPress = () => {
    Clipboard.setString(referralCode);
    setCopied(true);
  };

  return (
    <TextInput
      label={intl.formatMessage({defaultMessage: 'My Referral Code'})}
      value={copied ? 'Copied' : referralCode}
      right={
        <Touchable style={styles.copyBtn} onPress={_handleCopyPress}>
          <Text
            color={Colors.secondary}
            fontFamily="Poppins-SemiBold"
            textTransform="uppercase">
            {intl.formatMessage({defaultMessage: 'Copy'})}
          </Text>
        </Touchable>
      }
      editable={false}
      placeholder={intl.formatMessage({defaultMessage: 'Your referral code'})}
      containerStyle={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    paddingBottom: 24,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: Colors.placeholder,
  },
  copyBtn: {
    height: 50,
    justifyContent: 'center',
  },
});

const mapStateToProps = state => ({
  user: getUser(state),
});

/* Export
============================================================================= */
export default connect(mapStateToProps)(ReferralMyCode);
