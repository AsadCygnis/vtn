import {createSelector} from 'reselect';
import {firestoreTimestampToDate} from '../../util/functions';

/**
 * Get error
 */
export const getError = state => state.Referral.error;

/**
 * Get loading
 */
export const getLoading = state => state.Referral.loading;

/**
 * Get all ids
 */
export const getAllIds = state => state.Referral.allIds;

/**
 * Get byId
 */
export const getById = state => state.Referral.byId;

/**
 * Get month
 */
const getMonth = (state, {month}) => month;

/**
 * Get referral ids by month
 */
export const getReferralIdsByMonth = createSelector(
  [getAllIds, getById, getMonth],
  (allIds, byId, month) =>
    allIds
      .filter(id => {
        const date = firestoreTimestampToDate(byId[id].createdAt);
        const displayMonth = date.getMonth() + 1;
        const referralMonth = `${date.getFullYear()}-${
          displayMonth < 10 ? '0' + displayMonth : displayMonth
        }-01`;
        return referralMonth === month;
      })
      .sort(
        (a, b) =>
          firestoreTimestampToDate(byId[a].createdAt) -
          firestoreTimestampToDate(byId[b].createdAt),
      ),
);

/**
 * Get id
 */
const getId = (state, {id}) => id;

/**
 * Get referral by id
 */
export const makeGetReferralById = () =>
  createSelector([getById, getId], (byId, id) => byId[id]);

/**
 * Get reward by month
 */
export const getRewardByMonth = createSelector(
  [getReferralIdsByMonth, getById],
  (ids, byId) => ids.map(id => byId[id].reward || 0).reduce((a, b) => a + b, 0),
);
