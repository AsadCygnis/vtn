import firebaseAuth from '@react-native-firebase/auth';

import * as constants from './constants';
import {UsersCollection} from '../../config/firebase';

/**
 * REFERRALS_GET
 */
export const getReferrals = month => async dispatch => {
  try {
    dispatch({type: constants.REFERRALS_GET.REQUEST});

    const uid = firebaseAuth().currentUser.uid;
    const start = new Date(month);
    const end = new Date(month);
    end.setMonth(start.getMonth() + 1);

    const referralsSnapshot = await UsersCollection.doc(uid)
      .collection('referrals')
      .where('createdAt', '>=', start)
      .where('createdAt', '<', end)
      .get();

    const referrals = [];
    referralsSnapshot.forEach(snapshot => {
      if (snapshot.exists && snapshot.data().level === 1) {
        referrals.push({
          id: snapshot.id,
          ...snapshot.data(),
        });
      }
    });

    const usersSnapshot = await Promise.all(
      referrals.map(referral => UsersCollection.doc(referral.user).get()),
    );
    usersSnapshot.forEach((snapshot, i) => {
      if (snapshot.exists) {
        referrals[i]._user = {
          ...snapshot.data(),
          id: snapshot.id,
        };
      }
    });

    dispatch({
      type: constants.REFERRALS_GET.SUCCESS,
      payload: referrals,
    });
  } catch (error) {
    dispatch({type: constants.REFERRALS_GET.FAIL, error});
  } finally {
    dispatch({type: constants.REFERRALS_GET.COMPLETE});
  }
};
