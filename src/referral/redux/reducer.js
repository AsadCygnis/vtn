import * as constants from './constants';
import * as helpers from '../../util/reduxHelpers';

export const INITIAL_STATE = {
  byId: {},
  allIds: [],
  error: null,
  loading: false,
};

export default function reducer(state = INITIAL_STATE, action) {
  const {type, payload, error} = action;

  switch (type) {
    // REFERRALS_GET
    case constants.REFERRALS_GET.REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
      };
    case constants.REFERRALS_GET.FAIL:
      return {
        ...state,
        error,
      };
    case constants.REFERRALS_GET.SUCCESS:
      return helpers.merge(state, payload);
    case constants.REFERRALS_GET.COMPLETE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
