import React, {useState} from 'react';
import {FormattedMessage} from 'react-intl';

import {Container, StackHeader} from '../../common';
import MyRewardsList from '../components/MyRewardsList';
import MyRewardsSortOrderSelect from '../components/MyRewardsSortOrderSelect';

/* =============================================================================
<MyRewardsScreen />
============================================================================= */
const MyRewardsScreen = () => {
  const [sortOrder, setSortOrder] = useState('descending');

  return (
    <Container>
      <StackHeader title={<FormattedMessage defaultMessage="My Rewards" />} />
      <MyRewardsSortOrderSelect
        sortOrder={sortOrder}
        onSortOrderChange={setSortOrder}
      />
      <MyRewardsList sortOrder={sortOrder} />
    </Container>
  );
};

/* Export
============================================================================= */
export default MyRewardsScreen;
