import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
// import {useNavigation} from '@react-navigation/native';
import {FormattedMessage} from 'react-intl';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {
  Content,
  Container,
  Touchable,
  ModalHeader,
  ComingSoonModal,
} from '../../common';
import ReferralMyCode from '../components/ReferralMyCode';
import ReferralRewards from '../components/ReferralRewards';
import ReferralUsers from '../components/ReferralUsers';
import GiftIcon from '../../assets/icons/edit-gift.svg';
import * as Colors from '../../config/colors';
import {getMonthsBetween} from '../../util/functions';

/* =============================================================================
<ReferralScreen />
============================================================================= */
const ReferralScreen = () => {
  const insets = useSafeAreaInsets();
  // const navigation = useNavigation();
  const styles = getStyles(insets);
  const [month, setMonth] = useState(MONTHS[MONTHS.length - 1].label);
  const [comingSoonModal, setComingSoonModal] = useState(false);

  const _toggleComingSoonModal = () => {
    setComingSoonModal(prevState => !prevState);
  };

  const _handleGiftPress = () => {
    _toggleComingSoonModal();
    // navigation.navigate('MyRewards');
  };

  return (
    <Container>
      <ModalHeader
        title={<FormattedMessage defaultMessage="Referral" />}
        right={
          <Touchable
            style={styles.giftIconContainer}
            onPress={_handleGiftPress}>
            <GiftIcon fill={Colors.text} />
          </Touchable>
        }
      />
      <Content style={styles.content}>
        <ReferralMyCode />
        <ReferralRewards
          month={month}
          months={MONTHS}
          onMonthChange={setMonth}
        />
        <ReferralUsers month={month} />
        <ComingSoonModal
          visible={comingSoonModal}
          onCancel={_toggleComingSoonModal}
        />
      </Content>
    </Container>
  );
};

const TODAY = new Date();
const MONTHS = getMonthsBetween(new Date('2021-01-01'), TODAY);

const getStyles = insets =>
  StyleSheet.create({
    content: {
      paddingBottom: insets.bottom || 20,
    },
    giftIconContainer: {
      width: 33,
      height: 33,
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 20,
      borderRadius: 4,
      backgroundColor: Colors.card,
    },
  });

/* Export
============================================================================= */
export default ReferralScreen;
