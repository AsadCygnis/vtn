import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import ReferralScreen from './ReferralScreen';
// import MyRewardsScreen from './MyRewardsScreen';

const Stack = createStackNavigator();

/* =============================================================================
<ReferralStack />
============================================================================= */
const ReferralStack = () => {
  return (
    <Stack.Navigator
      headerMode="none"
      screenOptions={TransitionPresets.SlideFromRightIOS}>
      <Stack.Screen name="ReferralHome" component={ReferralScreen} />
      {/* <Stack.Screen name="MyRewards" component={MyRewardsScreen} /> */}
    </Stack.Navigator>
  );
};

/* Export
============================================================================= */
export default ReferralStack;
